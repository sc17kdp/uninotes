#include "hci0.h"

class HELLO_PAGE : public HCI_PAGE{
	using HCI_PAGE::HCI_PAGE;
public: // overrides
	void show(){
		out() << "====================\n";
		out() << "   this is a page\n";
		out() << " press key to leave\n";
		out() << "====================\n";
	}
};

class HELLO_MENU : public HCI_MENU {
public:
	explicit HELLO_MENU(HCI_APPLICATION& ctx)
	    : HCI_MENU(ctx, "hello"), _page("example page") {
		add('b', &hci_beep);
		add(0x1b, &hci_esc);
		add('p', &_page);
		add('q', &hci_quit);
	}

public:
	void show(){
		out() << "this is the menu headline\n";
		HCI_MENU::show();
	}
	void enter(){
		while(true){
			try{
				HCI_MENU::enter();
				break;
			}catch( HCI_UP const&){
				continue;
			}
		}
	}
private:
	HELLO_PAGE _page;
};

class HELLO : public HCI_APPLICATION{
public:
	HELLO() : HCI_APPLICATION(), _main_menu(*this) {
	}
public:
	void show(){
		try{
			_main_menu.enter();
		}catch(HCI_ESCAPE const&){
			// somebody pressed esc. exiting.
		}
	}

private:
	HELLO_MENU _main_menu;
}; // HELLO

// the program starts here.
int main(int argc, char const *argv[])
{
	HELLO hello;
	return hello.exec();
}
