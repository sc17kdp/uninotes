#include "hci1.h"
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include "magic.h"

const unsigned limit=10;
const char spacebar=32;
const char escape=0x1b;

class PAGER : public HCI_PAGE {
public:
	PAGER(std::string what, HCI_APPLICATION& ctx)
		: HCI_PAGE(what), _ctx(ctx), _filename(what)
	{
	}
private:
	void show(){
		boost::filesystem::basic_fstream<char> file;
		file.open(_filename, std::ios_base::in);
		std::string line;

		unsigned pageno=0;
		while(true){
			out() << "==== " << _filename << " ====\n\n";
			for(unsigned i=0; i<limit; ++i){
				std::getline(file, line);
				out() << line << "\n";
			}
			out() << "\n";
			++pageno;
			_ctx.set_status("==== page " + std::to_string(pageno) +
			                " ==== space: continue, esc: leave");
			while(int n=getkey()){
				if(n==spacebar){
					clear();
					break;
				}else if(n==escape){
					throw HCI_LEAVE();
				}else{
					// set_status...?
				}
			}
		}
	}

private:
	HCI_APPLICATION& _ctx;
	std::string _filename;
};

class IS_TEXT{
public:
	IS_TEXT() : _handle(::magic_open(MAGIC_NONE)){
		::magic_load(_handle, NULL);
	}
public:
	bool operator()(std::string const& f){
		std::string type(::magic_file(_handle, f.c_str()));

	   return type.find("ASCII") != std::string::npos;
	}
private:
	magic_set* _handle;
}is_text;

class HELP : public HCI_ACTION{
public:
	HELP(HCI_APPLICATION& a) : _ctx(a) {}
private: // override
	void do_it(){
		_ctx.set_status("there is no help.");
	}
private:
	HCI_APPLICATION& _ctx;
};

class DIR_MENU : public HCI_MENU {
public:
	explicit DIR_MENU(HCI_APPLICATION& ctx, std::string what=".")
	    : HCI_MENU(ctx, what),
	      _what(what),
	      _ctx(ctx),
	      _help(ctx){ }

	~DIR_MENU(){ untested();
		while(!_stuff.empty()){
			delete _stuff.back();
			_stuff.pop_back();
		}
		clear();
	}

private:
	void populate(){
		purge();
		add(escape, &hci_esc);
		add('q', &hci_quit);
		add('?', &_help);
		if(_what!="."){
			add('u', &hci_up);
		}else{
		}
		unsigned diridx=0;
		unsigned fileidx=0;
		for (boost::filesystem::directory_iterator it(_what);
				it!=boost::filesystem::directory_iterator(); ++it){
			std::string myfile(it->path().string());

			if(is_directory(*it)){
				if(diridx<limit/2){
					_stuff.push_back(new DIR_MENU(_ctx, myfile));
					add('A'+diridx, _stuff.back());
					++diridx;
				}else{
				}
			}else if(is_text(myfile)){
				if(fileidx<limit/2){
					_stuff.push_back(new PAGER(myfile, _ctx));
					add('a'+fileidx, _stuff.back());
					++fileidx;
				}else{
				}
			}else{
			}
		}
	}
public:
	void enter(){
		while(true){
			try{ untested();
				HCI_MENU::enter();
				break;
			}catch(HCI_UP const&){ untested();
				continue;
			}
		}
	}
	void show(){
		populate();
		out() << "listing " << _what << "\n\n";
		HCI_MENU::show();
		out() << "\n";
	}
private:
//	DIR_MENU& _submenu;
	std::string _what;
	std::list<HCI*> _stuff;
	HCI_APPLICATION& _ctx;
	HELP _help;
};

class BROWSE : public HCI_APPLICATION{
public:
	BROWSE(int argc, char const *argv[])
		: HCI_APPLICATION("browse"), _main_menu(*this) {}
public:
	void show(){
		while(true){
			try{
				_main_menu.enter();
			}catch(HCI_ESCAPE const&){ untested();

			}
		}
	}
private:
	DIR_MENU _main_menu;
};

// the program starts here.
int main(int argc, char const *argv[])
{
	BROWSE hello(argc, argv);
	return hello.exec();
}
