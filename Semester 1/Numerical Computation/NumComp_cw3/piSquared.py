
def piSq(n):
    sum = 0.0
    for k in range(1, n+1):
        sum += 1/(k**2)
    sum *= 6
    return (sum)


piTable = {}
piTable["10^6"] = piSq(10**6)
piTable["10^7"] = piSq(10**7)
print(piTable)
piTable["10^8"] = piSq(10**8)
print(piTable)
piTable["10^9"] = piSq(10**9)
print(piTable)
