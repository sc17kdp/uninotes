
n = int(input("What is the value of n?\n"))
matrix = []
i = 0.0
j = 0.0
for i in range(1, n+1):
    row = []
    for j in range(1, n+1):
        entry = 1/((i+1)*(j+1))
        row.append(entry)
    matrix.append(row)

for row in matrix:
    print(row)
