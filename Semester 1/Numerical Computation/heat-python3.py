"""
% heat.py
%
% Finds the distribution of temperature throughout a square for a given
% temperature distribution on the boundary.
"""

        import numpy as np
        import matplotlib.pyplot as plt
        from mpl_toolkits.mplot3d import axes3d, Axes3D
        from matplotlib import cm
        from mpl_toolkits.mplot3d.axes3d import get_test_data
        from matplotlib import cm
        from matrixSolve import *
        import time

# time at the start of the program
# start = time.clock()
# Assign the value of the number of approximation points required in
# each direction
m = 33

# Initialise an array for storing the temperature at each approximation
# point in the m by m grid
Temp = np.zeros([m, m])

# Impose the desired temperature distribution on the boundary (any values
# not set here are left as zero).
Temp[0, :] = np.linspace(100, 0, m)
Temp[:, 0] = np.linspace(100, 0, m)

# Let n be the number of points in each direction at which th temperature
# is unknown and let n2 be the total number of unknown points (i.e. the
# number of grid points not on the boundary).
n = m-2
n2 = n**2

# Initialise to zero all entries of the n2 by n2 matrix and the right-hand
A = np.zeros([n2, n2])
b = np.zeros([n2, 1])

# Insert all of the non-zero entries into the matrix and right-hand side
# vector

# Begin with the row of the grid immediately below the top boundary.
i = 1
j = 1
k = 0  # k is used to store the row number of the matrix throughout.
A[k, k] = 4.
b[k] = b[k] + Temp[i-1, j]
b[k] = b[k] + Temp[i, j-1]
A[k, k+1] = -1.
A[k, k+n] = -1.

for j in range(2, m-2):
    k += 1
    A[k, k] = 4.
    b[k] = b[k] + Temp[i-1, j]
    A[k, k-1] = -1.
    A[k, k+1] = -1.
    A[k, k+n] = -1.

k += 1
A[k, k] = 4.
b[k] = b[k] + Temp[i-1, j]
A[k, k-1] = -1.
b[k] = b[k] + Temp[i, j+1]
A[k, k+n] = -1.

# Now consider rows 2 to m-2
for i in range(2, m-2):
    j = 1
    k += 1
    A[k, k] = 4.
    A[k, k-n] = -1.
    b[k] = b[k] + Temp[i, j-1]
    A[k, k+1] = -1.
    A[k, k+n] = -1.
    for j in range(2, m-2):
        k += 1
        A[k, k] = 4.
        A[k, k-n] = -1.
        A[k, k-1] = -1.
        A[k, k+1] = -1.
        A[k, k+n] = -1.

    k += 1
    A[k, k] = 4.
    A[k, k-n] = -1.
    A[k, k-1] = -1.
    b[k] = b[k] + Temp[i, j+1]
    A[k, k+n] = -1.

# Finally consider the row of the grid immediately above the bottom
# boundary
i = m-2
j = 1
k += 1
A[k, k] = 4.
A[k, k-n] = -1.
b[k] = b[k] + Temp[i, j-1]
A[k, k+1] = -1.
b[k] = b[k] + Temp[i+1, j]
for j in range(2, m-2):
    k += 1
    A[k, k] = 4.
    A[k, k-n] = -1.
    A[k, k-1] = -1.
    A[k, k+1] = -1.
    b[k] = b[k] + Temp[i+1, j]

k += 1
A[k, k] = 4.
A[k, k-n] = -1.
A[k, k-1] = -1.
b[k] = b[k] + Temp[i, j+1]
b[k] = b[k] + Temp[i+1, j]

# create a zero matrix
g = np.zeros([n2, 1])
# use the jacobi new function from matrixsolver
# u, it = jacobi_new(A, g, b, 10**-5)
tol = 10**-5
# w = 1.6
# u = sor_new(A, g, b, n2, tol, w)
u = gauss_seidel_new(A, g, b, tol)


# print(u)
# The entries of u can now be mapped into their corresponding points in
# the two-dimensional array Temp
k = -1
for i in range(1, m-1):
    for j in range(1, m-1):
        k += 1
        Temp[i, j] = u[k]

# Temp
print((Temp[int((m+1)/2-1), int((m+1)/2-1)]))

# How long the calculations took
# elapsed = (time.clock() - start)
# print(it)

# Finally we can plot the results
x = np.linspace(1, m, m)
y = np.linspace(1, m, m)
X, Y = np.meshgrid(x, y)

fig = plt.figure()
ax = Axes3D(fig)
surf = ax.plot_surface(X, Y, Temp, rstride=1, cstride=1,
                       cmap=cm.gnuplot, linewidth=0, antialiased=False)
# ax.plot_wireframe(X,Y,Temp)
ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_zlabel('Temperature')
ax.set_xlim3d([1, m])
ax.set_ylim3d([1, m])
ax.set_zlim3d([0., 100.])
plt.title('Temperature distribution across a square')
# print("Time elapsed: ", elapsed)
plt.show()
