from nonlinear_functions import sqrt2


def secant(fnon, x0, x1, tol):
    """
    Use the secant method to find the root of the nonlinear equation fnon(x)=0
    starting from the estimates x0 and x1.
    ARGUMENTS:  fnon handle for the nonlinear function
                x0 the initial estimate
                x1 the second estimate
                tol convergence tolerance
    RETURNS:    x the computed root
                f the function value at that root
    """
    # Print column headings for output
    print('      x              f(x)\n')

    f_x0 = fnon(x0)
    f_x1 = fnon(x1)

    # # Print the estimate and function value
    print(' %12.14f %12.14f' % (x1, f_x1))

    # Repeat the  iteration until the magnitude of the function value is
    # less than tol.
    while abs(f_x1) > tol:
        try:
            x = x1 - (f_x1 * (x1 - x0)/(f_x1 - f_x0))
        except ZeroDivisionError:
            print("Error! - denominator zero for x = ", x)

        x0 = x1
        x1 = x
        f_x0 = f_x1
        f_x1 = fnon(x1)

        # Print the new estimate and function value.
        print(' %12.14f %12.14f' % (x1, f_x1))


secant(sqrt2, 1.4, 1.5, 1.0e-10)
