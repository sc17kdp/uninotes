---
title: SEP Notes
author: Krishan Patel
---
# Waterfall
![Waterfall Image](Images/Waterfall.png)

1. **Requirements analysis and definition** The system’s services, constraints, and
goals are established by consultation with system users. They are then defined
in detail and serve as a system specification.

2. **System and software design** The systems design process allocates the require-
ments to either hardware or software systems by establishing an overall system
architecture. Software design involves identifying and describing the fundamen-
tal software system abstractions and their relationships.

3. **Implementation and unit testing** During this stage, the software design is real-
ized as a set of programs or program units. Unit testing involves verifying that
each unit meets its specification.

4. **Integration and system testing **The individual program units or programs
are integrated and tested as a complete system to ensure that the software
requirements have been met. After testing, the software system is delivered to
the customer.

5. **Operation and maintenance** Normally (although not necessarily), this is the
longest life cycle phase. The system is installed and put into practical use.
Maintenance involves correcting errors which were not discovered in earlier
stages of the life cycle, improving the implementation of system units and
enhancing the system’s services as new requirements are discovered

It should only be used when the requirements are well understood and unlikely to change radically during system development. 

## Problems
Because of the costs of producing and approving documents, iterations can be costly and involve significant rework. Therefore, after a small number of iterations, it is normal to freeze parts of the developments, such as the specification and to continue with the later development stages.

Problems are left for later resolution, ignored, or programmed around.

This premature freezing of requirements may fmean that the system won't do what the user wants.

It may also lead to badly structured systems as design problems are circumvented by immplementation tricks.

Its major problem is the inflexible partitioning of the project into distinct stages. Commitments must be made at an early stage in the process, which makes it difficult to respond to changing customer requirements.


## Incremental Development compared to Waterfall

1. The cost of accommodating changing customer requirements is reduced. The amount of analysis and documentation that has to be redone is much less than is required with the waterfall model.

2. It is easier to get customer feedback on the development work that has been done. Customers can comment on demonstrations of the software and see how much has been implemented. Customers find it difficult to judge progress from software design documents.

3. More rapid delivery and deployment of useful software to the customer is possible, even if all of the functionality has not been included. Customers are able to use and gain value from the software earlier than is possible with a waterfall process.

## Problems with incremental development
From a management perspective, the incremental approach has two problems:

1. The process is not visible. Managers need regular deliverables to measure progress. If systems are developed quickly, it is not cost-effective to produce documents that reflect every version of the system.

2. System structure tends to degrade as new increments are added. Unless time and money is spend on refactoring to improve the softwarae, regular change tends to corrupt its structure. Incorporating further software changes becomes increasinigly difficult and costly.

The problems become particularly acute for large, complex, long-lifetime systems, where different teams develop different parts of the system. Large systems need a stable framwwork or architecture and the responsibilities of the different teams working on parts of the system need to be clearly defined with respect to that architecture. This has to be planned in advance rather than developed incrementally.





