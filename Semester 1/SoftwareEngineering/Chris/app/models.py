from app import db


class Task(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    taskName = db.Column(db.String(500))
    description = db.Column(db.String(2000))
    endDate = db.Column(db.DateTime(), nullable=False)
    status = db.Column(db.Boolean(), nullable=False)
