from flask_wtf import Form
from wtforms import StringField, TextField, SubmitField, FieldList, FormField, BooleanField
from wtforms.validators import DataRequired
from wtforms.widgets import TextArea
from wtforms.fields.html5 import DateField

class NewTaskForm(Form):
    taskName = StringField('Task name', validators=[DataRequired()])
    description = StringField(validators=[DataRequired()], widget=TextArea())
    endDate = DateField('Due date', format='%Y-%m-%d', validators=[DataRequired()])
    submit = SubmitField('Create task')
