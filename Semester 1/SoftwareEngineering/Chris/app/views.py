from app import app
from flask import render_template, redirect, url_for, flash
from .forms import NewTaskForm
from .models import Task
from app import db, models
from sqlalchemy import update


@app.route('/')
def index():
    return redirect(url_for('viewTasks'))


@app.route('/viewTasks')
def viewTasks():
    # Quesries the database for all active tasks, rendering them.
    # If there are no results, just flash the user a message.
    allTasks = []
    isData = False
    for task in models.Task.query.filter_by(status=False).all():
        taskArray = [task.id, task.taskName, task.description, task.endDate]
        allTasks.append(taskArray)

    if len(allTasks) != 0:
        isData = True
    else:
        flash('No active tasks.. create one!', 'success')

    return render_template('viewTasks.html', data=allTasks, isData=isData)


@app.route('/viewTasks/MarkComplete/<id>')
def MarkTaskComplete(id):
    # Takes the ID of a task and removes it from the database, then reloads the home page
    task = Task.query.get(id)
    task.status = True
    db.session.commit()
    flash('Changes successfully made!', 'success')
    return redirect(url_for('viewTasks'))


@app.route('/createNew', methods=['GET', 'POST'])
def NewTask():
    # Render the form, validates the informaiton when submit and renders the view tasks if successful
    form = NewTaskForm()

    ##################### REMOVE ############################
    print(form.endDate.data, form.taskName.data, form.description.data)
    if form.validate_on_submit():
        newTask = Task(taskName=form.taskName.data, description=form.description.data,
                       status=False, endDate=form.endDate.data)
        db.session.add(newTask)
        db.session.commit()

        flash(
            f'Task {form.taskName.data} has been successfully added!', 'success')
        return redirect(url_for('viewTasks'))
    return render_template('createNew.html', form=form)


@app.route('/viewCompleteTasks')
def viewCompleteTasks():
    # Queries the database for all tasks marked as complete and renders the results
    allTasks = []
    for task in models.Task.query.filter_by(status=True).all():
        taskArray = [task.id, task.taskName, task.description, task.endDate]
        allTasks.append(taskArray)

    return render_template('viewCompleteTasks.html', data=allTasks)
