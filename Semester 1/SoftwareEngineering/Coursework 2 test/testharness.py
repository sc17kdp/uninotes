from flask_table import Table, Col

# set up the table and set the columns


class ItemTable(Table):
    time = Col('Time')
    mon = Col('Monday')
    tues = Col('Tuesday')
    weds = Col('Wednesday')
    thurs = Col('Thursday')
    fri = Col('Friday')


# Get some objects
class Item(object):
    # initialise the variables
    def __init__(self, time, mon, tues, weds, thurs, fri):
        self.time = time
        self.mon = mon
        self.tues = tues
        self.weds = weds
        self.thurs = thurs
        self.fri = fri


# data in the timetable
items = [Item('9:00', 'lecture1', 'lecture 2', 'lecture 3', 'lecture 4', 'lecture 5'),
         Item('10:00', 'lecture1', 'lecture 2',
              'lecture 3', 'lecture 4', 'lecture 5'),
         Item('11:00', 'lecture1', 'lecture 2', 'lecture 3', 'lecture 4', 'lecture 5')]


# return the timetable
def getTimetable():
    table = ItemTable(items)
    message = """ {% extends "layout.html" %}
    {% block page_title %}Timetable{% endblock %}
    {% block body %}
    {{ super() }}
    <div class="col-lg-6">
        <h3>Weekly Timetable</h3>""" + table.__html__() + """</div>
    {% endblock %}"""

    return message
