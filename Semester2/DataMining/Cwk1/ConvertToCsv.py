import pandas as pd
import numpy as np

tsv_file = 'start-kit-offenseval/training-v1/offenseval-training-v1.tsv'
csv_table = pd.read_table(tsv_file, sep='\t')
csv_table.to_csv('offenseval-training-v1.csv', index=False)
