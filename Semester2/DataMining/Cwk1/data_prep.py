with open("offenseval-training-v1.tsv") as file:
    new_file = open("training.csv", 'a')
    for line in file:
        columns = line.split("\t")
        cleaned_tweet = columns[1].replace('\"', '\\\"')
        new_line = "{},\"{}\",{}\n".format(columns[0], cleaned_tweet, columns[2])

        new_file.write(new_line)
    new_file.close()
