
from __future__ import print_function
from copy import deepcopy
# Representation of a state:
# (number_of_queens_placed,  full_board_state)


def queen_get_initial_state(x, y):
    return (0, matrix_of_zeros(y, x))


def matrix_of_zeros(X, Y):
    return [[0 for x in range(X)] for y in range(Y)]


def queen_possible_actions(state):
    # Get a list of all the queens
    queens = getQueens(state)
    moves = []
    for i in range(BOARD_X):
        for j in range(BOARD_Y):
            available = True
            # Checks if a square has a queen in it
            available = checkSquare(i, j, queens)
            if available:
                moves += [[i, j]]
    return moves


def checkSquare(i, j, queens):
    for queen in queens:
        if i == queen[0] and j == queen[1]:
            return False
    return True


def getQueens(state):
    queens = []
    # Get a list of the current queens
    for i in range(BOARD_X):
        for j in range(BOARD_Y):
            if state[1][i][j] == 1:
                queens += [[i, j]]
    return queens


def queen_successor_state(action, state):
    board = deepcopy(state[1])
    board[action[0]][action[1]] = 1
    # increase the number of queens placed on the board
    numQueens = state[0] + 1
    # return a state
    return (numQueens, board)


def queen_goal_state(state):
    # Get a list of all the queens
    queens = getQueens(state)
    for i in range(BOARD_X):
        for j in range(BOARD_Y):
            covered = False
            # Check if each square is covered by a queen or not
            covered = isCovered(i, j, queens)
            if covered == False:
                return False
    # print out the board if the problem is solved
    print_board_state(state)
    return True


def isCovered(i, j, queens):
    for queen in queens:
        if i == queen[0] or j == queen[1] or checkDiagonal(i, j, queen):
            return True
    return False


def checkDiagonal(i, j, queen):
    # Checks if a square is covered by a queen on the diagonal
    return abs(float(j-queen[1])/float(i-queen[0])) == 1


def print_board_state(state):
    # prints out the state of the board
    board = state[1]
    print("\nThe board state:")
    for row in board:
        for square in row:
            print(" %2i" % square, end='')
        print()


def queen_print_problem_info():
    print("The Queen's Tour (", BOARD_X, "x", BOARD_Y, "board)")


def make_qc_problem(x, y):
    global BOARD_X, BOARD_Y, queen_initial_state
    BOARD_X = x
    BOARD_Y = y
    queen_initial_state = queen_get_initial_state(x, y)
    return (None,
            queen_print_problem_info,
            queen_initial_state,
            queen_possible_actions,
            queen_successor_state,
            queen_goal_state
            )