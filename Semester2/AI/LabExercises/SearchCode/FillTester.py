
import sys
from tree          import *
from queue_search  import *
from FillProblem  import *

problem = make_fill_problem(100)

search(problem, 'depth_first', 110, ['loop_check'])
