---
title: Lecture 5
---

# Adverserial Games

Games that are:

- two players
- deterministic
- zero sum
- perfect information

e.g. Chess, Go, Noughts and Crosses, Checkers

## Min and Max

Two players: Min and Max. Play alternates, Max starts.

Also called Player (Max) and Opponent (Min)

Utility (minimax)  function gives utility for Max. High utility values are good for Max; low values good for
min.

Often draw trees with $\bigtriangleup$ for Max turn and $\bigtriangledown$ for Min turn.

## Strategy

How is searching a game tree different from the search problems we saw earlier?

Compare solving 8-tile puzzle with playing noughts and crosses. In eacch case there is a 3x3 grod
with markings for eacch state and each action changes the markings on the grid. Looks very similar.

Not just looking for seqquence of actions leading to a goal state for Max.

**Strategy**: Meed to find best move for Max initially, then what response Max should make to each of
the possible moves by Min, and so on.


If utliity values are known at each stage, the players can just choose the best move for them (for
max that is the highest, for min it is the smallest).

In most cases, the utility value is only given for the final nodes of the game.

### Minimax (utility) values

Recursion to the leaves and backing up the utility values: depth-first exploration of game tree.

For depth *m* and branching *b* the time complexity is $O(b^m)$

This time complexity makes the algorithm impractical in realistic examples, but it is important as
other algorithms are based on this simple idea.

### Properties of minimax

**Complete?** Yes, if tree is finite (chess has specific rules for this.

**Optimal?** Yes against an optimal opponent.

**Time complexity?** $O(b^m)$

**Space complexity?** $O(bm)$

For chess b = 35, m = 100 for "reasonable" games. The exact solution is completely infeasible.

## $\alpha - \beta$ pruning

Stops you having to explore every path.

The value of the root and hence the minimax deccision are independent of the values of the pruned
leaves $x$ and $y$.

### General case

If m is better than n for Player, we will never get to n in play.

Better values are determined as follows

- $\alpha$ = the vlaue of the best (i.e. highest value) choice we have found so far at any choice
  point along the path for MAX.
- $\beta$ = the value of the best (i.e. lowest-value) vhoice we have found so far at any choice
  point along the path for MIN .






