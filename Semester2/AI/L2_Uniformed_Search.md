---
title: Uniformed Search
---


# Implementation Components
All standard search algorithms treat the search space as a tree, start at root
and terminate in leaf nodes/ 

Each node has a state, a parent, children reachable by actions and a cost.

Node expansion is the process of computing the children of a given node. Done
in two stages

1. Determine what actions are possible
2. Determine the successor state that results from each of thesse actions

A **state** is a representation of a physical configuration

A **node** is a data structure constituting part of a search tree, includes
parent, children, depth.

Collection of nodes generated but not processed: **fringe**

Collection of states in already processed nodes: **explored**

**BFS**: remove oldest from fringe

* Expand shallowest unexpanded node
* fringe is a FIFO queue
	* i.e. new successors go at end

**DFS**: remove newest from fringe

* Expand deepest unexpanded node
* fringe is a LIFO queue (Stack)
	* i.e put successor at front

## Strategies are evaluated along the following dimensions:

* completeness - does it always find a solution if one exists
* time complexity - number of nodes generated/ expanded
* Space complexity - maximum number of nodes in memory
* Optimality - does it always find the least cost solution?

Time and space complexity are measured in terms of:

* b - maximum branching factor of the search tree
* d - depth of the least-cost solution
* m - maximum dpth of the state space (may be infinite)

## Modifications
**BFS**

* uniform cost
* Bi-directional search

Can be combined with removal of duplicate states and other node pruning
techniques

**DFS**

* Depth Limited
* Iterative Deepening

### Depth Limited Search
Incomplete unless every branch is eventually terminated either by:
* reaching a goal or dead end
* looping to a state that occurs earlier in the branch (we need to test for
  this).

If there are non-terminating branches, the search may run forever.

In a depth limited search we seet a max length of path that will be searched

It is usually still incomplete. However, at least the search algorithm is
guaranteed to terminate.


### Iterative Deepening
Consists of repeated use of depth limited or depth first search, where the
depth limit is increased each time until a solution is found.

Combines benefits of breath and depth first search:

* Modest memory requirements (same as depth first)
* Complete when branching factor is finite
* Optimal when path cost is non-decreasing function of depth

Downside is that execution time is likely to be large.


### Uniform Cost Graph Search
* Remove node from fringe which has least cost path from the start.
* **EXPAND**(node, problem) can include nodes with the same state as a node
  already in fringe. These nodes might give lower cost to reach same state, so
nodes in the fringe are replaced in this case.

If the cost of all actions is the same, Uniform Cost Search is equivalent to
standard breath first search

## Direction of Search
We havev been thinking of search as progressing from an initial state towards a
goal state.

It is sometimes better to search backwards. We start from the goal and then
look for states that can reach the goal by one action. Look at states that can
reach these states by one action, etc until we reach the initial state

Useful when the backwards branching factor is lower than the forwards branching
factor. Backwards search is difficult to implement if there are many goal
states, or if the state transitions are difficult to define in the reverse
direction.

**Bi - Directional Search** is a strategy where the search space is explored
from both the initial state and the goal state. A solution is found when the
forward and backwards search paths meet at some state. 














