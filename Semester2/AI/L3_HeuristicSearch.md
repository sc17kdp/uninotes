---
title: Lecture 3
---

# Informed (Heuristic) Search

This uses information about the problem beyond the definition of the problem

Can tell whether one state is 'closer' to the goal than another.

In uninnformed search can only tell if a state is a goal or not, we may know the cost from the start
to an intermediate state - but we have no idea of the cost of getting from the intermediate state to
the goal.

Informed search uses a heuristic function *h(n)* to estimate the cost of getting from node n to the
goal.


## Algorithms
**g(n)** is the distance from the start

* Greedy best-first: remove node with lowest h(n)
* A\*: remove node with lowest *h(n) + g(n)*

Uniform cost(not heuristic): remove node n with lowest g(n)


### Greedy Best-First
Like uniform cost except that:

* removed node n has least value of *h(n)*
* doesn't need to replace any nodes in **fringe** as heuristic depends only on the state and cannot
  give different values for different nodes having same state.

Not optimal

### A\* Search

Optimal under certain conditions

Like uniform cost except:

* removed node *n* has least value of **h(n) + g(n)**
* replace nodes in **fringe** using *h(n) + g(n)* as measure of cost instead of just *g(n)*

**Optimal under these conditions**

* Each state in the search space has finitely many successors
* All actions have costs greater than some value $\epsilon$ > 0
* Admissibility: The heuristic function never overestimates cost to a goal node. Underestimating is
  ok: if *h(n) = 0* for all n, then A\* is just uniform cost.

Under these conditions, and provided there is a path of ffinite cost to a goal state from the start
the A\* algorithm will terminate with minimal cost path to a goal.

**Proof that A star is optimal **

```
fringe = [start]
found = false

while fringe not empty and not found:
	n = 1st thing in fronge
	if n is a goal then found = true
	else
		expand n and add successors to fringe
		order fringe by at least f = g + h value

if found then trace back to root in tree and return answer
else stop with failure

```

Let P be the path found by A\* from the start S to goal G, made by a sequence of $n_i, 1 <= i <= m$
nodes and $G = n_m.$ We will show that:

1. the actual cost of P, $g(n_m)$ is lower than the estimated cost f of any path through any node
   such as n' in the fringe
2. But since those estimates are optimistic, A\* can safely ignore those nodes. In other words it
   will never overlook the possibility of a lower-cost path and so is admissible.

**Proof**

* To prove the first part: P is made by subsequent selection of $n_i$ points from the fringe
  containing n'. This implied that: $\forall n_i, f(n_i) = g(n_i) + h(n_i) < f(n')$. However, at the
goal we have $h(n_m = G) = 0$. Thus the cost of P, i.e $g(P) = f(G)$ is also smaller than $f(n')$. 
* To see the second part, because h is admissible and always gives the lowest estimate, 
$f(n') = c( S' \rightarrow n') + h(n') < g(P')$. Thus, we can conclude that: $g(P) < g(P')$ hence, P
is the optimal path. 
