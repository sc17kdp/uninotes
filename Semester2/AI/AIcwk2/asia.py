# Initialization from a BayesianModel object
from pgmpy.factors.discrete import TabularCPD
from pgmpy.models import BayesianModel
from pgmpy.inference import VariableElimination
from GibbsSamplingWithEvidence import GibbsSampling
import numpy as np
import argparse
import re
import math

# This parses the inputs from test_asia.py (You don't have to modify this!)

parser = argparse.ArgumentParser(description='Asia Bayesian network')
parser.add_argument('--evidence', nargs='+', dest='eVars')
parser.add_argument('--query', nargs='+', dest='qVars')
parser.add_argument('-N', action='store', dest='N')
parser.add_argument('--exact', action="store_true", default=False)
parser.add_argument('--gibbs', action="store_true", default=False)
parser.add_argument('--ent', action="store_true", default=False)

args = parser.parse_args()

print('\n-----------------------------------------------------------')

evidence={}
for item in args.eVars:
    evidence[re.split('=|:', item)[0]]=int(re.split('=|:',item)[1])

print('evidence:', evidence)

query = args.qVars
print('query:', args.qVars)

if args.N is not None:
    N = int(args.N)


# Using TabularCPD, define CPDs
cpd_asia = TabularCPD(  variable='asia', variable_card=2, 
                        values=[[0.99], [0.01]])

cpd_tub = TabularCPD(   variable='tub', variable_card=2, 
                        values=[[0.99, 0.95],[0.01,0.05]],
                        evidence=['asia'], evidence_card=[2])

cpd_bron = TabularCPD(  variable='bron', variable_card=2, 
                        values=[[0.7, 0.4], [0.3, 0.6]], 
                        evidence=['smoke'], evidence_card=[2])

cpd_either = TabularCPD(variable='either', variable_card=2, 
                        values=[[0.999, 0.001, 0.001, 0.001],
                                [0.001, 0.999, 0.999, 0.999]], 
                        evidence=['lung', 'tub'], evidence_card=[2, 2])

cpd_smoke = TabularCPD(variable='smoke', variable_card=2, values=[[0.5], 
                                                                [0.5]])

cpd_lung = TabularCPD(  variable='lung', variable_card=2, 
                        values=[[0.99, 0.9], [0.01, 0.1]], 
                        evidence=['smoke'], evidence_card=[2])

cpd_xray = TabularCPD(  variable='xray', variable_card=2, 
                        values=[[0.95, 0.02], [0.05, 0.98]], 
                        evidence=['either'], evidence_card=[2])

cpd_dysp = TabularCPD(  variable='dysp', variable_card=2, 
                        values=[[0.9, 0.2, 0.3, 0.1],
                                [0.1, 0.8, 0.7, 0.9]], 
                        evidence=['either', 'bron'], evidence_card=[2, 2])


# Define edges of the asia model
asia_model = BayesianModel([('asia', 'tub'),
                            ('tub', 'either'),
                            ('either', 'xray'),
                            ('either', 'dysp'),
                            ('smoke', 'lung'),
                            ('lung', 'either'),
                            ('smoke', 'bron'),
                            ('bron', 'dysp')])

# Associate the parameters with the model structure.
asia_model.add_cpds(cpd_asia, cpd_tub, cpd_bron, cpd_either, cpd_smoke, cpd_lung, cpd_xray, cpd_dysp)

# Find exact solution if args.exact is True:

exactDict = {}

def getExact(x,asia_infer):
    return asia_infer.query([x],evidence=evidence)

def findExact():
    print("Exact Inference:")
    asia_infer = VariableElimination(asia_model)
    for x in query:
        exact = getExact(x,asia_infer)
        exactDict[x] = exact.values[1]
        print(x,": ",exact.values[1])

# Find approximate solution and cross entropy if args.gibbs is True:

approxDict = {}
# count how many are true for the query variable in the gibbs sampling
# normalise the value 
def getApprox(samples, x):
    sum = 0
    for i in range(0,len(samples)):
        sum+= samples[x][i]
    return sum/N

def findApprox():
    print("Approximate posterior probabiliy for N =",N)
    # get the gibbs sample
    asia_sampler = GibbsSampling(asia_model)
    samples = asia_sampler.sample(size=N,evidence=evidence)
    # for each query variable get the approx posterior probability
    for x in query:
        approx = getApprox(samples, x)
        approxDict[x] = approx 
        print(x,": ",approx)

def getTCE(approx, exact):
    # add the running tce value
    return ((1-approx)*np.log10(1-exact)) + (approx*np.log10(exact))

def crossEntropy():
    tce = 0
    # add up the tce from the exact and approx values
    for x in query:
        exact = exactDict[x]
        approx = approxDict[x]
        val = getTCE(approx, exact)
        tce += val
    # make the TCE positive
    tce *= -1

    print("Total Cross-Entropy: ",tce)

# call the correct function depending on the arguments given
if args.exact:
    findExact()
if args.gibbs:
    findApprox()
if args.ent and args.gibbs and args.exact:
    crossEntropy()

print('\n-----------------------------------------------------------')
