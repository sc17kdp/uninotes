---
title: Lecture 1 introduction
---
# Computability
The study of computation, the power of computation and the limits on the power
of computation

# What is an algorithm?
A set of rules for getting a specific output from a specific input. Each step
must be so precisely defined it can be translated into computer language and
executed by a machine. - Donald Knuth in Scientific American, 1977.

## Another definition
Hartley Rogers Theory of recursive funtions and effective computability,
McGraw-Hill, 1967

* An algorithm is given as a set of instructions of finite size
* There is a computing agent which can react to the instructions and carry out
  the computations
* There are facilities for making , storing, and retrieving steps in a
  compuation
* The computing agent reacts to the set of instructions in such a way that for
  a given input the compuation is carried out in a discrete *stepwise* fashion,
without any use of continuous methods or analogue devices
* The computing agent reacts to the set of instructions in such a way that a
  computation is carried forward *deterministically* without resort to random
methods or devices e.g. dice
* Is there to be a fixed finite bound on the size of inputs?
* Is there to be a fixed finite bound on the size of a set of instructions?
Is there to be a finxed finite bound on the *amount of storage space
available?*
  
# Models of computation
We study computation by examining various abstract *models of compuation,* and
in particular "machines"

these models do represent real computers and, surprisingly, are just as
powerful.

Both low level (machine code) models and high level (Python-like) models exist.

## Examples
* **Turing machines**
* Equational calculus
* lamda calculus
* **Partial recursive functions**
* **Production systems (grammars)**
* Unlimited Register machine

# Church's Thesis
All these models of compuation are of exquivalent power, and each filly
captures the intuitive notion of compuatable. Furthermore, no other model of
compuation can compute more.

This tells us that if we can prove a general property to be true of any of
these models of compuation, and is a general *characteristic* of "compuation".

