---
title: Lecture 6
---

# Regular Expressions

**Definition**: A language is **regular** on an alphabet $\Sigma$ if it can be denoted by a regular
expression on $\Sigma$.

**Theorem**: If a language is regular on an alphabet $\Sigma$, then the language is accepted by some
NFA with input alphabet $\Sigma$ with $\lambda$ - moves and only one accepting state

**Proof**: By induction on the definition of the set of regular languages on $\Sigma$

**Basis**

(i) The regular language $\emptyset$ is acceptied by the FA

(ii) The regular language {$\lambda$} is accepted bt the FA

(iii) For every $\sigma \in \Sigma$, the regular language $\{\sigma\}$ is accepted by a FA of the
form start $\rightarrow_\sigma$ f.

**Induction Hypothesis**

Let R be a regular language and assume that R is accepted by the FA. Also let S be a regular
language and assume that S is accepted by the FA.

**Induction Step**

We must define NFAs with $\lambda$ - moves which accept the languages $R \cup S, RS, and, R^*$


**Removing $\lambda$ transitions**
To complete the proof of Kleene's Theorem we must prove that the language accepted by an NFA with
$\lambda$ - transitions can be accepted by another NFA which does not have any
$\lambda$-transitions.

The easiest way of doing this is by a 'brute force' method: if it is possible to get from state p to
state q in an NFA with $\lambda$ - transitions A using only the single symbol $\sigma$ from the
input alphabet, then we define the new transition $\sigma_B (p, \sigma) = q$ in the new NFA without
$\lambda$-transitions B. Otherwise the new FA is the same as the old one (except that if $\lambda$
is accepted by A then we make the start state accepting in B).

(slide 12)

