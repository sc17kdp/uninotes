---
title: Lecture 14
---

# The Cantor Pairing Function

This function simply encodes a pair of numbers as a single number.

Cantor: $\mathbb{N}^2 \rightarrow \mathbb{N}$

Cantor(m,n) = $0.5 * (m + n) (m + n + 1) + m$

To number Statement Lists we use...

## The Fundamental Theorem of Arithmetic

Any natural number greater than one can be expressed as a unique product of prime numbers (unique,
that is, except for the order of the primes).

This is called unique *prime factorisation*.

So we can define a function Godel to code statement lists.
$$Godel(S_1;S_2;...;S_n) = p_1^{Code(S_n)} \times p_2^{Code(S_{n-1})} \times .... \times
p_n^{Code(S_1)}$$

Where $p_i$ is the $i^{th}$ prime number

## The Partial Recursive Functions

$\varphi_n$ is the function computed by the Basic While Program which is numbered $n$. So
$$\varphi_0, \varphi_1, \varphi_2, ....$$

is a listing of all the commputable functions. These are called *partial recursive functions*.

Notes:

- The word 'recursive' just means computable here - not the usual meaning of recursive.
- These functions are not all different, as two different programs may compute the same function.

## The halting problem

"Given any one input Basic While Program P (e.g. a turing machine) and any natural number k, decide
whether or not P will ever terminate when it is given k as input."
 
- The decision must be made usign one decision procdeure which works for every P and every k.
- The decision procedure must be algorithmic (i.e. computable)

It is undecidable.

**Theorem**

The function halt: $\mathbb{N} \times \mathbb{N} \rightarrow \{0,1\}$ defined by:

$halt(x,y) = \begin{cases} 1 & \quad \text{if } \varphi_x(y) \downarrow \\ 0 & \quad \text{otherwise} \end{cases}$

Proof on slide 11

## A Universal Program

**Theorem**

There exists a two-input Basic While Program U which can "simulate" every one input Basic While
Prgram. That is, if $$f_u: \mathbb{N}^2 \rightarrow \mathbb{N}$$ is the function computed by U, then
for every pair of natural numbers e,n, $$f_u(e,n) \simeq \varphi_e(n)$$ 
where e is the number of the program and n is the input into the program.

**Outline Proof**

In order to prove this theorem we need to construct the program $U$. Note that U does not have any
elaborate data structures available to it, but only the natural numbers.

Here is a basic outline of U.

read(e,n);

decode $e = Cantor_3(i,j,k);$

$x_i := n;$

factorise $j = p_1^{m_q} \times .... \times p_q^{m_1}$

decode the first statement on $m_1$ & simulate it;

decode the $2^nd$ statement $m_2$ & simulate it;

.

.

.

decode the last statement $m_q$ & simulate it;

write($x_k)$

**Theorem**

The function $g: \mathbb{N} \rightarrow \mathbb{N}$ defined by 
$$g(x)= \begin{cases} 1 & \quad \text{if } \varphi_x \text{is total} \\ 0 & \quad \text{otherwise}
\end{cases}$$

is not computable.

Proof. Suppose, if possible, that g is computable. Then the function h: $\mathbb{N} \rightarrow
\mathbb{N}$ defined by

$$h(x) = \begin{cases} f_u(x,x) + 1 & \quad \text{if } g(x) = 1 \\ 0 & \quad \text{if } g(x) = 0
\end{cases}$$

**Definition**

A recursive function is a partial recursive function which is total.

**Collary**

There can be no model of computation which precisely characterises the recursive functions.

(LECTURE 15 SLIDE 2 CARRY ON) 
