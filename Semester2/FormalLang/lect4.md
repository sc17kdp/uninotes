---
title: Lecture 4
---

# Regular Expressions

Regular Expressions represent languages.

\+ represents OR
 
\* represents any number of occurances 

## Definition by induction

Basis:

The expression $\theta$ denotes the language $\theta$

The expression $\lambda$ denotes the language \{$\lambda$\}

For every $\sigma$ $\in$ $\Sigma$, the expression $\sigma$ denotes the language \{$\sigma$\}

Induction:

If the regular expressions m and n denote the languages M and N then:

**m + n** denotes the language M $\cup$ N,

**m n** denotes the language M N and

m\* denotes the language M\*

## Operations on Languages

Let M and N be languages over the alphabet $\Sigma$

**Def of language concatenation**
$$ M N = \{m \frown n : m \in M  \&  n \in N\}$$

**Def of Kleene Closure**
The Kleene Closure of M, written M\*, is the language on $\Sigma$ defined by induction as:

Basis: $\lambda \in M\*$
Induction: 

$if w \in M\* and m \in M then w \frown m \in M\*$


## Kleene's Theorem

Let L be a language on an alphabet $\Sigma$.

L can be represented by a regular expression on $\Sigma$ if and only if there is a finite automaton
with input alphbet $\Sigma$ which accepts L

That is, these two very different models of computation compute precisely the same languages. The
two models of computation have the same power.

The proof.

A proof of this result would take the form:

If a language L is accepted by a non-deterministic finite automaton then L is accepted by a
deterministic finite automaton then L can be denoted by a regular expression then L is accepted by a
non-deterministic finite automaton with $\lambda$-transitions then L is accepted by a
non-deterministic finite automaton.

As these implications start and end at the same place we will have proved that all the named models
of compuation are equivalent.

## Non-Determinism
A compuation is *non-deterministic* if there is a point in the compuation at which there is no
uniquely defined next step.

Determinism is a fundamental property of ocmputations, but nevertheless non-determinism has proved
to be a useful property in computer science

- in abstracting from very complicated deterministic processes
- In early stages of algorithm design - in some cases non-deterministic algorithms are easier to
  design or more structured; they can be transformed into deterministic algorithms at a later stage
- Complexity analysis of problems
- In modelling situations in which one has incomplete information on, or incomplete control over,
  certain events in the environment

To a certain extent, whether an algorithm is deterministic or non-deterministic can depend upon the
level of abstraction at which one views the algorithm

Non-determinism and russian roulette - *spin* represents spinning the cylinder and pulling the
trigger. *swap* represents swapping the gun and pulling the trigger.

### Non-Deterministic Computations

A compulation is usually a sequence of states, each following deterministically, and in particular
uniquely from the one that goes before.

A *non-deterministic compuation* will form a tree, as it will *branch* at certain points in the
compuation.

Each branch in the tree of compuations continues independently of all the others (and so it follows
that the branches can never join together again). 

### Non-Deterministic Finite State Automata

A finite state automaton is non-deterministic if for some pair of state and input there is more
 than one possible transition defined.

In terms of a state transition diagram, there will be two arcs with the same label, leaving the 
same node, but going to different nodes.

A word is accepted by a non-deterministic finite state automaton if when started in the start 
state with that word as input, any of the branches in the non-deterministic computation tree end up 
in an accepting state.


