---
title: Lecture 12
---

# The Turing Machine

A turing machine, like a finite automaton, has a set of states, an input alphabet, a state
transition function and a start state.

However, it also has a **tape** with no start or end which is divided into squares. The Turing
Machine can both read from and write to the tape.

At any point in a computation the turing machine is above a particular square, *reading* the symbol
which is "stored in" that square.

## The syntax of the Turing machine

$$<Q, \Gamma, \Sigma, \Delta, \delta, B, q_0>$$

Q is a finite set of states.

$\Gamma$ is the finite tape alphabet

$\Sigma$ is the input alphabet, satisfying $\Sigma \subseteq \Gamma$

$\Delta$ = {L,R}

B is the blank symbol and B $\in \Gamma$

$q_0$ is the start state and $q_0 \in$ Q

$\delta$ is the state transition function. It is a partial fnuction and has the functionality
$$ \delta: Q \times \Gamma \rightarrow Q \times \Gamma \times \Delta$$

The values of the state transition function are often written in the form $$(p, \sigma, q,
\gamma, d) \in \delta$$

## The Semantics of the Turing Machine

If the turing machine is in state p and is reading the symbol $\sigma$ on the current square on the
tape, and $$\delta (p, \sigma) = (q, \gamma, d)$$

Then the machine writes the symbol $\gamma$ on the current square, moves into the state q, and
movves its tape head to reading the square either to the right or the left, depending on whether d
is L or R.

If $\delta (p, sigma)$ is undefined then the computation halts.

## Conventions for Input and Output

To compute on input w $\in \Sigma^*$, this string of symbols is written in somme undivided string of
squares, and a compuation is started in the start state reading the leftmost non-blank symbol on the
tape.

If the Turing Machine ever stops then the output is the string of symbols between the leftmost
symbol that is not blank and the rightmost symbol that is not blank.

If the Turing mahcine never stops then there is no output for that input.

## A turing machine computes a function

So a turing machine computes a function f. If the Turing Machine is started with w written on the
tape then either the turing machine eventually stops with z written on the tape in which case f(w) =
z or the Turing machine never stops and f(w) is undefined and we writght f(w) $\uparrow$

## Computing Functions on Natural Numbers

The standard approach is to represent the natural numbers in unary (i.e. base 1)

If a function has more than one argument then we separate these arguments on the tape by one special
symbol.

(Note that the Turing Machine could equally well compute in binary or denary, unary is just the
normal approach as it is easier)

