---
title: Lecture 17
---

# Rice's Theorem

**Semantics of While Programs** - The semantics of the Basic While Program P which has number e is
the function $\varphi_e$

## Semantic Properties

A semantic property of Basic While Programs is a set of programs that satisfies the property that if
two programs are semantically equivalent (i.e. they compute the same function) then either both of
them are in the set, or neither of them are.

**Formal definition** - A semantic property of Basic While Programs is a set of natural numbers
satisfying the property that $$(\forall m \in \mathbb{N} ) (\forall n \in \mathbb{N}) [m \in P \&
\varphi_m \simeq \varphi_n \Rightarrow n \in P]$$

### Examples of Semantic Properties 

- The set of all programs which compute addition
- The set of all programs which terminate on all inputs
- The set of all programs which compute some (any one ) given function


## Rice's Theorem

If P $\subseteq \mathbb{N}$ is a semantic property of while programs and P $\neq \emptyset$ and P
$\neq \mathbb{N}$ then P is not a recursive set.

Corollary

Any non-trivial semantic property of programs is undecidable and therefore not compile time
checkable.

# Fin
