---
title: Lecture 11
---

# The limitations of Finite Automata: The Pumping Lemma

Used to prove that a language is not regular.

For any language L which is regular on an alphabet $\Sigma$ there exists a constant $n \in N$ such
that for all $z \in L$ with $|z| > n$ there exists $u,v,w \in \Sigma^*$ satisfying

- z = uvw (just split z up into three bits where z is a word in L)
- |uv| <= n (number of states)
- |v| >= 1
- $(\forall i \in N)uv^iw \in L$

## How to prove a language is not regular

Proposition
The language L = {$0^i 1^i | i \in N$} is not regular on the alphabet {0,1}.

**Proof by contradiction**

Suppose, if possble, that it is regular and let n be the number which the Pumping Lemma tells us
exists. As |$0^n1^n$| > n, z = $0^n1^n$ is a candidate for the z in the Pumping Lemma. So there must
exist u,v,w $\in \{0,1\}^*$, satisfying uvw = $0^n1^n$, |uv| <= n and v $\neq \lambda$. Now as |uv|
<= n, uv must consist of entirely of 0's and so v must consist entirely of 0's. So $uv^2w$ must have
more than n 0's, but it still has only n 1's. So $$uv^2w \notin \{0^i1^i | i \in N\} = L$$ Which
contradicts the Pumping Lemma so the language is not regular.

(PROOF OF PUMPING LEMMA ON SLIDE 4)

## Finite Automata that Compute Functions

$$<Q, \Sigma, \Delta, \delta, \mu, q_0>$$

Q is a set of states

$\Sigma$ is the input alphabet

$\Delta$ is the output alphabet

$\delta$: $Q \times \Sigma \rightarrow Q$ is the state transition function

$\mu$ is the output function

$q_0$ is the start state

If $\mu: Q \rightarrow \Delta$ then it is called a **Moore machine**

If $\mu: Q \times \rightarrow \Delta$ then it is called a **Mealy machine**

$\delta$ and $\mu$ must be total functions.


**Finite automata cannot compute multiplication**: 

This is because the number of states required to store the "carries" increases 
with the size of the numbers, and so no matter how many states our automaton has, we can always find two 
numbers which require more states to multiply them.

This is not a proof, but it forms the basis of a proof like the Pumping Lemma Proof.


