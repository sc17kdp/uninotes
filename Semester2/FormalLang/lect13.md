---
title: Lecture 13
---

# Computability: An Introduction to Recursion Theory

# The Partial Recursive Functions

The functions on the natural numbers which can be computed by Turing Machines are called the
*partial recursive functions*.

The Church Turing Thesis states that *any* function that can be computed on the natual numbers can
be computed by a Turing Machine.

Note that there are uncountably many functions on the natural, numbers, but only coutably many
Turing Machines, and so there are lots of functions that cannot be computed.

## Basic while programs

**Basic while programs** are just as powerful as Turing Machines, and so according to Church's
Thesis they can compute all functions which can be computed. However, they are much more easy to
work with than Turing Machines.

Start with a **read()** and end with a **write()**. None in the middle.

### The BNF

\<BasicWhileProgram\> ::= \<ReadCommand\> ; \<StatementList\> ; \<WriteCommand\>

\<StatementList\>::= \<Statement\> | \<StatementList\> ; \<Statement\>

\<Statement\> ::= \<WhileStatement\> | \<AssignmentStatement\>

\<WhileStatement\> ::= **while** \<Identifier\> $\neq$ 0 **do** \<StatementList\> **od**



## Programs Compute Functions

If the \<IdentifierList\> is in the \<ReadCommand\> of a \<BasicWhileProgram\> contains k
identifiers then that program, in a well understood fashion, computes a function $$f: \mathbb{N}^k
\rightarrow \mathbb{N}$$ 

If the program does not terminate when given a certain input, say 24,9, then f(24,9) is not defined
and we write $f(24,9) \uparrow$

In this case the function computed by the program is a *parital* function.

## An Uncomputable Function

Theorem

Let us assume that we can number the one-input Basic While Programs 1st, 2nd, 3rd... and let $\varphi_k$ be the function computed by the kth one-input basic while program.

Then the following function is not computable by a Basic While program as it *diagonalises* out of
the comutable functions.

$$\alpha(n) = 
\begin{cases}
\varphi_n(n) + 1 	& \quad \text(if ) \varphi_n(n) \downarrow \\ 0 & \quad \text(otherwise)
\end{cases}$$ 

Proof on slide 11

slide 14 is **unexaminable**

## Numbering Programs

To each Basic While Program we will assign a number.

We will give a (commputable) procedure for calculating the number of a program. Furthermore, form a
given number we will be able to construct the program that that number represents.

So in some sense we will reduce programs to just numbers (or we will compile programs onto numbers.)

