---
title: Lecture 3
---

# Semantics for Finite Automata
The difference between $\delta$ and $\hat{\delta}$ 

* $\delta$ operates on symbols only where as $\hat{\delta}$ operates on words
* $\delta$ specifies a given FA and is different for every FA; the definition of $\hat{\delta}$ in terms of
 $\delta$ is the same for every FA.
* $\hat{\delta}$ defines the semantics of the model whereas $\delta$ defines
  the syntax of a given FA.
* $\delta$ defines what the FA is and $\hat{\delta}$ defines what it does

For $\sigma$ $\in$ $\Sigma$ and q $\in$ Q $$\hat{\delta}(q, \sigma) = \delta(q,\sigma)$$

Proof
$$\hat{\delta}(q,\sigma) = \hat{\delta}(q, \lambda \frown \sigma) $$


$$ = \delta(\hat{\delta}(q,\lambda), \sigma) $$
$$=  \delta(q,\sigma) $$

The first line is the definition of $\lambda$, the empty word.

The second line is the inductive part of the definition of $\hat{\delta}$

The last line is the basis part of the definition of $\hat{\delta}$

Any symbol in an alphabet is also a word on that alphabet: $\Sigma \Subset \Sigma$\*

$\uparrow$ means that it is not defined

A function that is not defined for all possible inputs is called a partial function.

A total functiton is one that is defined on all possible arguments.

## Dead states
A dead state is a non accepting state which satifies the condition that for every input symbol
$\sigma$, $$\delta(d, \sigma) = d$$ where $\delta$ is the state transition function


## Lemma
For any finite automaton A there is an equivalent finite automaton B in which the state transition
function is total.

**Sketch of proof**

Construct B from A by adding a dead state d, and replacing undefined transitions in A by transitions
into d in B. That is if $\delta_A(q,\sigma) \uparrow$ then define $\delta_B(q,\sigma) = d$.

To complete the proof we have to show that L(A) = L(B)



