---
title: Lecture 5
---

# Removing Non-Determinism from an NFA

Non deterministic vs Deterministic finite automata

- More modular - important property of good designs
- Easier to design

Want to design using an NFA then automatically transform NFA into DFA.

(slide 2 is a NFA, slide 3 is a DFA)

## Create DFA B from a NFA A
Each node in *B* will correspond to a set of nodes from A.
Start with the start node $q_0$ of a and let \{$q_0$\} be the start state of B. Taking an input
symbol $\sigma$ we form a new state in B which is the set of all states A can enter if $\sigma$ is
applied to $q_0$ in A.
Then one by one we apply all the other input symbols to $q_0$, forming new states in B.
Then one by one we apply all the input symbols to all the neww state4s we have formed in B.
We keep on doing this until we have exhausted all the new states and input symbols.
A state in B is accepting if it "contains" any accepting state from A.



## The formal definition of NFA

A NFA $$<Q,\Sigma,\delta,F,q_0>$$ is a finite automaton in which the state transition function
is non-deterministic.

The language accepted by a non deterministic FA is defined by:
$$ L(A) = \{w \in \Sigma^ * |(\exists q \in Q)(\hat{\delta}(q_0,w) = q \& q \in F) \}$$

**Theorem**: for any NFA there is an equvalent deterministic FA.

Construction: Given an NFA $$A = <Q,\Sigma, \delta, F, q_0>$$

define a deterministic FA $$B= <Q_B, \Sigma, \tau,G,\{q_0\}>$$

where $$Q_B = \{S|S \subseteq Q\}$$

and $$G = \{S|S\subseteq Q \&F \cap S \neq \emptyset\}$$

and $$\tau (S, \sigma) = \{p|(\exists q) ((q\in S)\&\delta (q, \sigma) = p)\}$$

Where S is a state **(like me)**

We omit the proof that A and B are equivalent!


## DFAs give regular expressions

So we have seen that a language accepted by an NFA can be accepted by a DFA.

The next step in the proof of Kleene's Theorem is to prove that the language accepted by a DFA can
be denoted by a regular expression. For any two states p and q in a DFA A, let $L_A (p,q)$ be the
set of words that will transform p to q. Then we can prove by induction on the number of states in
the automaton that $L_A (p,q)$ is regular, and so it follows that the language is accepted by the
automaton is regular. However, we omit the proof.


**Regular Expressions ive NFAs with $\lambda$ - moves.**

The next step in the proof of Kleene's Theorem is to prove that the language denoted by a regular
expression can be accepted by an NFA with $\lambda$ - moves. We prove this by induction on the
definition of a regular expression.

An NFA with $\lambda$ moves is like an ordinary NFA except that it is also allowed to make state
stransitions between inpputs (or with no input). That is, the functionality of the state transition
function is extended to $$\delta : Q \times (\Sigma \cup \{\lambda\}) \rightarrow Q$$
