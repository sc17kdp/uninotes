---
title: Lecture 2
---

# Finite automaton
State transistion diagrams

* Circles represent states
* Arrows represent transitions
* The arrow labels represent input symbols

The *finite automaton *represents a computation as a sequence of *states*,
where the next state is defined *deterministically* given the current state and
the input

## The definition of the Finite Automaton
A finite automaton is a 3-tuple <Q,$\Sigma$,$\delta$>

Where Q is a finite set of states, sigma is a finite input alphabet and
delta is the definition

$\delta$: Q x $\Sigma$-> Q

## The finite automaton as a language acceptor

* Make one of the states the **start state** (sometimes pointed at by an arrow)
* Make some of the states **accepting states** (indicated by a double circle
* A word is accepted by the FA if by starting in the start state and inputting
  the symbols in the word, one by one, the finite automaton ends up in an
accepting state
* The start state can be an accepting state
* There can be many accepting states but only one start state


### Finite state Acceptors
A 5-tuple
$<Q, \Sigma, \delta, F, q_0>$
where <Q, $\Sigma$, $\delta$> is a finite automaton, F $\Subset$ Q and $q_0$
$\in$ Q.

$q_0$ is called the **start state** and F is the set of **accepting states**.

Q is the states set ${q_0, q_1,q_2, q_3}$
 
$\Sigma$ is the input alphabet
F is the set of one accepting state $q_0$
$q_0$ is the start state


## Alphabets, words and languages
Definition - an *alphabet* is a set of symbols

A *word* on an alphabet is a string of symbols from the alphabet

$\frown$ is the concatenation symbol that is: $$00 \frown 11 = 0011$$

$\lambda$ is the empty word defined by w $\frown$ $\lambda$ = $\lambda \frown$
w = w

$\Sigma$\* is the set of all workds on the alphabet $\Sigma$.

A is a language on an alhpabet $\Sigma$ is A $\Subset$ $\Sigma$\*

The **language accepted by a finite automaton** is the set of all workds on
the input alphabet which take the finite automaton from the start state into an
accepting state.

For any finite state acceptor A = <Q, $\Sigma$, $\delta$, F, $q_0$>

L(A) = {w $\in$ $\Sigma$\* | $\hat{\delta}$($q_0$, w) $\in$ F}

**Definition of $\hat{\delta}$**: For any finite automaton <Q, $\Sigma$,
$\delta$> define $$\hat{\delta}: Q x \Sigma\* -> Q$$
by induction of the efinition of $\Sigma$\*

Basis: $\hat{\delta}$(q, $\lambda$) = q

Induction for $\sigma$ $\in$ $\Sigma$ and w $\in$ $\Sigma$\* 
$$\hat{\delta}(q, w \frown \sigma) = \delta(\hat{\delta}(q,w),\sigma)$$

