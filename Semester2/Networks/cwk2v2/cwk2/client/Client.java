
import java.net.*;
import java.io.*;

public class Client {

	private Socket socket = null;
	private PrintWriter socketOutput = null;
	private BufferedReader socketInput = null;

	public void startClient() {

		try {

			// try and create the socket
			socket = new Socket("localhost", 8888);

			// chain a writing stream
			socketOutput = new PrintWriter(socket.getOutputStream(), true);

			// chain a reading stream
			socketInput = new BufferedReader(new InputStreamReader(socket.getInputStream()));

		} catch (UnknownHostException e) {
			System.err.println("Don't know about host.\n");
			System.exit(1);
		} catch (IOException e) {
			System.err.println("Couldn't get I/O for the connection to host.\n");
			System.exit(1);
		}
	}

	public void list() {
		startClient();
		// write to server
		String fromServer;
		try {
			socketOutput.println("list");
			while ((fromServer = socketInput.readLine()) != null) {
				// echo server string
				System.out.println("Server: " + fromServer);
			}
			socketOutput.close();
			socketInput.close();
			socket.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void get(String fname) {
		startClient();

		try {
			socketOutput.println("get");
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void put(String fname) {
		startClient();
		try {
			socketOutput.println("put" + fname);
			File fileToSend = new File("clientFiles/" + fname);
			System.out.println(fname);
			socketOutput.println(fileToSend.length());

			if (!fileToSend.exists()) {
				System.out.println("File doesn't exist");

			} else {
				System.out.println("File exists");
			}

			byte[] byteArray = new byte[(int) fileToSend.length()];

			BufferedInputStream bis = new BufferedInputStream(new FileInputStream(fileToSend));
			OutputStream os = socket.getOutputStream();
			int trxBytes = 0;
			while ((trxBytes = bis.read(byteArray, 0, byteArray.length)) != -1) {
				os.write(byteArray, 0, trxBytes);
				System.out.println("Transfering bytes : " + trxBytes);

			}
			// os.flush();
			bis.close();
			socket.close();

			System.out.println("File Transfered...");
		} catch (Exception e) {
			System.out.println("Client Exception : " + e.getMessage());
		}

	}

	public static void main(String[] args) {
		String command = args[0];
		Client kkc = new Client();
		String fname = null;
		// list - lists all the files in serverFiles
		if (command.equals("list")) {
			kkc.list();
			// call the list command
		}
		// get fname - retrieves fname and saves it in clientFiles
		if (command.equals("get")) {
			fname = args[1];
			// call the get command
		}
		// put fname - sends fname and saves it in serverFiles
		if (command.equals("put")) {
			fname = args[1];
			kkc.put(fname);
			// call the put command
		}

		kkc.startClient();
	}
}