
import java.net.*;
import java.nio.Buffer;
import java.io.*;

public class Client {

	private Socket socket = null;
	private PrintWriter socketOutput = null;
	private BufferedReader socketInput = null;
	private OutputStream os = null;
	private InputStream is = null;

	public void list() {
		// write to server
		String fromServer;
		try {
			socketOutput.println("list");
			while ((fromServer = socketInput.readLine()) != null) {
				// echo server string
				System.out.println("Server: " + fromServer);
			}
			socketOutput.close();
			socketInput.close();
			socket.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void startClient() {

		try {

			// try and create the socket
			socket = new Socket("localhost", 8888);

			// chain a writing stream
			os = socket.getOutputStream();
			socketOutput = new PrintWriter(os, true);

			// chain a reading stream
			is = socket.getInputStream();
			socketInput = new BufferedReader(new InputStreamReader(is));

		} catch (UnknownHostException e) {
			System.err.println("Don't know about host.\n");
			System.exit(1);
		} catch (IOException e) {
			System.err.println("Couldn't get I/O for the connection to host.\n");
			System.exit(1);
		}
	}

	public static void main(String[] args) {
		String command = args[0];
		Client client = new Client();
		String fname = null;
		client.startClient();
		try {
			// list - lists all the files in serverFiles
			if (command.equals("list")) {
				// call the list command
				client.list();
			}
			// get fname - retrieves fname and saves it in clientFiles
			if (command.equals("get")) {
				// call the get command
				fname = args[1];
				client.get(fname);

			}
			// put fname - sends fname and saves it in serverFiles
			if (command.equals("put")) {
				// call the put command
				fname = args[1];
				client.put(fname);

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void get(String fname) throws IOException {
		int bytesRead;
		FileOutputStream fos = null;
		BufferedOutputStream bos = null;
		int FILE_SIZE = 6022386; // DONT HARD CODE THIS IN
		try {
			socketOutput.println("get");
			socketOutput.println(fname);
			byte[] mybytearray = new byte[FILE_SIZE];
			InputStream is = socket.getInputStream();
			fos = new FileOutputStream("clientFiles/" + fname);
			bos = new BufferedOutputStream(fos);
			bytesRead = is.read(mybytearray, 0, mybytearray.length);

			bos.write(mybytearray, 0, bytesRead);
			bos.flush();
			System.out.println("File " + fname + " downloaded (" + bytesRead + " bytes read)");
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (fos != null)
				fos.close();
			if (bos != null)
				bos.close();
			if (socket != null)
				socket.close();
		}
	}

	public void put(String fname) throws IOException {
		BufferedInputStream bis = null;
		BufferedOutputStream bos = null;
		int FILE_SIZE = 6022386;
		try {
			socketOutput.println("put" + fname);
			File fileToSend = new File("clientFiles/" + fname);
			System.out.println(fname);
			// sends file size to the servers
			socketOutput.println(fileToSend.length());

			if (!fileToSend.exists()) {
				System.out.println("File doesn't exist");

			} else {
				System.out.println("File exists");
				byte[] mybytearray = new byte[FILE_SIZE];
				bis = new BufferedInputStream(new FileInputStream(fileToSend));
				bis.read(mybytearray, 0, mybytearray.length);

				bos = new BufferedOutputStream(os);
				System.out.println("Sending " + fname + "(" + mybytearray.length + "bytes)");
				int count;
				while ((count = bis.read(mybytearray)) > 0) {
					bos.write(mybytearray, 0, count);
				}
				bos.write(mybytearray, 0, mybytearray.length);
				bos.flush();
				System.out.println("Done.");

				System.out.println("File Transfered...");
			}

		} catch (Exception e) {
			System.out.println("Client Exception : " + e.getMessage());
		} finally {
			if (bis != null)
				bis.close();
			if (bos != null)
				bos.close();
			if (socket != null)
				socket.close();
		}

	}

}