---
title: Lecture 11
---

# Thread - pool model

Baic approach is to have a **fixed number of threads** to handle clients

- known has **thread-pool model**
- Also called **Threads-per-server**, as the number of threads in set by the server (and *not* the
  clients.

Threads **queue** to accept clients

- The first `accept()` blocks all later servers (until another client makes a connection).

Behaves like a **set** of servers with the same listening port.

## Implementation

In essence, every thread runs a version of our earlier one-client-per-server model:

1. Create a number of threads at the start of the application
2. Each thread listens to the **same listening port** (possible since these are threads *not*
   processes).
3. Each thread calls the blocking `accept()`
4. When this method returns, it will handle **one client at a time**


## Thread-pool server

Still only one `ServerSocket` which is shared by all thread.

Therefore still only one **listening port**

`poolSize` could be a static instance variable, set by command line arguments in main(), etc.

## Thread-pool behaviour

- The pool size is a fixed parameter of our model
- Each thread is a **sequential** server running `accept()`
- The threads are **queued** at the listening port.
- Which thread returns from `accept()` depends on the JVM and its thread scheduler. 
	- Not predictable; may vary from run to run
- When a client connection is ended, the corresponding thread returns to the start of its while-loop
  and calls `accept()`
	- It has 'returned to the pool'

## Thread safety

Note that multiple threads call `accept()` on the **same** `ServerSocket` object.

Does `accept()` perform properly in this situation?

- i.e. will it only return the `Socket` object to one thread, or might it return the same client to
  two or more threads?

In other words, is `accept()` **thread safe**?

The official documentation does not make it clear if `accept()` is thread safe.

- Some implementations of JVM may be, some may not.

As a general rule, if a class of not explicitly declared thread safe, it is best to assume it is
**not**.

## Thread-safe accept()

If we were worried about this we could use synchronised(...).

This way, only one thread can call `accept()` at a time.

- Thread-safety of `accept()` no longer required.
- Which thread enters the synchonised block first is still unpredictable.

## Pros

- Can handle multiple clients connecting to the same port
- The required resource is finite and controllable.

## Cons

- Must **tune** the pool size to the expected load
	- Too small, and clients will have to wait.
	- Too large, and the overhead will increase and all threads will slow down.

Ideally we would **dynamically** alter the pool size to match the load.

# Executor Server

Ideally we would ike to **re-use** existing threads if they are all 'idle', and onnly create a new
thread if all current ones are in use.

This could take some time to develop using `java.lang.thread`

However, because it is useful in many contexts, Java already provides the high-level **Executor**
service that does momst of the work for us.

- Found in `java.util.concurrent`

## java.util.concurreent

`java.lang.Thread` is actually quite a low-level detail of concurrency.

Java 5 introduced a **higher-level** programming model:

- The java.util.concurrent package
- Decouples **scheduling ** and **execution**.
- Allows us to manage the use of threads, and leave the details of creation/destruction to
  elsewhere.

Assume the JVM handles this sensibly, so the performance is 'good enough'.

## newCachedThreadPool

The `newCachedThreadPool` creates a **flexible** pool that:

- **Creates new threads** if current ones are in use.
- **Re-uses** existing threads if they have completed their task.

It therefore **automatically adjusts** to the load (i.e. the number of clients).

There is also a `newFixedThreadPool(..)`, which sets the pool size.

- Similar to our thread-pool model, with the same cons.
- Only requires one line of code to change between `newCachedThreadPool` and `newFixedThreadPool`

### Using newChackedThreadPool

A factory method that returns an instance of ExecutorService.

Usage is straightforward:

```
ExecutorService service =  Executors.newCachedThreadPool()
```

- Executors can return different objects, including cached and fixed-size thread pools.
- ExecutorService is generic (polymorphism)

`service.submit(new KKClientHandler(client))`

- Schedules the executionof the task.
- submit's argument implements the Runnabe interface, i.e. a Thread.

## Pro's

Only creates new threads when existing resources are exhausted.

Can switch to a fixed pool by changing one line.

## Con's

The pool is still unbounded, so may exhaust machine resources (go out of memory, or run very slow)

In particular, **long lived clients** keep their resources


# Choosing an architecture

What determines what arrchitecture we should use?

**Application requiremtns**

- Protocol (e.g. storage required, compute-intensive)
- Expected number of conntections at a time

**User behaviour**

- Length of connection
- Typical amount of communication per connection.

Should be careful to serparate **design** (i.e. client server architecture) from **communication**
(i.e. client server protocol)

- Can re-use **same** architecture for different protocols.


