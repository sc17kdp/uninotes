---
title: Lecture 9
---

# Parallel Computing

## Problems with the simple server

accept() is **blocking**:

- It only returns once a client makes contact
- In the meantime, the entire server application is doing nothing

Only communicated with **one client at a time**:

- Other clients will get queued, leading to delays.
- Server may be frequently idle during communication with the client
- Only one protocol handler per server, rather than one per client (e.g. KnockKnockProtocol)

## Parallel computation

Software that makes use of hardware capable of performing calculations 'in parallel'. An
increasingly important skill, as almost all modern hardware is parallel.

Moores law: Number of transistors on a chip doubles every 18-24 months. 

Although the law still holds, processor speeds no longer track it. The problem is heat generation
increases rapidly with clock speed.

\+ One way to improve performance is to allow calculations to be performed **simultaneously**, i.e.
in parallel.

### The limits of automated parallelism

**Instruction-level parallelism (ILP)** processors use a *pipelining* architecture in which each
instruction is processed in multiple stages, somewhat like an assembly line at a factory.

- Different stages for subsequent instructions can be performed simultaneously.

CPUs also have **multiple functional units** such as FPUs and ALUs that can work independently.

- **Superscalar** architectures can merge nearby calculations into a single one.

However each of these have limits - they do not **scale**.


### Current parallel architectures

Almost all modern architectures contain multiple **processing units**, e.g. cores, multiple CPUs etc

- **Scales** better than the previous automated parallelism
- Software must be **specifcally developed** to take into these processing units.

Current parallel hardware can be broadly classified into:

#### Shared memory architectures.

**Definition**: All processing units (e.g. cores) access the same memory.

This includes anything with one or more multi-core CPUs, so almost all modern desktops, laptops,
tablets, smart phones etc.

#### Distributed memory architectures

**Definition**: Processing units only access a fraction of total memory available

Includes **High-Performance Computing** (HPC) clusters (e.g. supercomputers), and **distributed
systems** ('cloud computing')


#### Graphics Processing Units or GPUs

Hardware specifically designed to rapidly perform calculations that arise in the graphical rendering
of scenes

Historically driven by graphics applications, especially video games, but increasing being used for
**non-graphical** applications such as machine learning, cryptocurrencies etc

- Some devices design for general purpose calculations. Known as GPGPUs - General purpose GPUs

GPU hardware has multiple memory stores, some of which are distributed, some shared.

### The brain

Most complex system known. If regarded as a computer it would be massively parallel

Synapse speeds are about 5ms so the clock speed would be < 1kHz. We have about $10^{11}$ neurons, each
connected to $10^4$ others. The current fastest supercomputer has $10^7$ cores.

### How to program parallel architectures?

Parallel architectures allow multiple calculations to be performed **simultaneously**.

- Can improve performance without increasing clock speed.

However, what if one calculation requires the result of *another* calculation as input? How can they
be performed in parallel? They can't.

Algorithms tyoically need to be reesigned to make good use of parallel hardware. These new
algorithms need to be designed and implemented by **programmers**.

### Concurrency

Concurrency is when two or more tasks are in progress in the same time frame.

Foor instance, for an event-driven GUI, a user event (e.g. a mouse click) might result in a callback
function being called. 

- Would normally be called by a seperate task (thread), so that the main loop can continue (and
  detect other user events.

Can be achieved using **interrupts**:

- OS slices CPU time amongst all running tasks
- Most commonly **pre-emptive** multi-tasking

### Parallelism

An increasingly important concept related to concurrency is parallelism.

Parallelism refers to the ability to perform multiple calculations simultaneously by using more than
one processing unit.

Possible to be concurrent but not parallel:

- Multi-tasking is possible on a single core CPU, which only has one compute unit (i.e. the core)

However, cannot be parallel and not concurrent.

If something is parallel then it must be concurrent. But if something is concurrent it doesn't have
to be parallel.


### Processes vs Threads

There are two basic units of execution, **processes** and **threads**.

Processes:

- A self contained execution environment
- Private set of run-time resources
- Has its own heap memory
	- All objects created with *new* (inc arrays), global variables etc
- Also has its own stack memory, which has local variables, function arguments etc.
- Expensive to create and destroy.
- Normaly 1 application = 1 process, but not always:
	- For example, each tab in an application may correspond to a different process.


Threads:

- Are launched by, and exist within, a process.
- Every process has at least one (the main thread in java)
- Have their own stack memory.
- Shares the heap memory with the launching process.
	- Potential problems if multiple concurrent threads can read and write the same data.
- Is **lightweight**; small cost for creating and destroying threads.
- How threads are assigned to cores is up to the OS, more specifically the **scheduler**.
	- For compute-intensive parallel programs, would normally want one thread per core.

 
