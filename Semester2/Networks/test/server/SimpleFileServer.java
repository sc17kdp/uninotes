import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class SimpleFileServer {

  public final static int SOCKET_PORT = 13267; // you may change this
  public final static String FILE_TO_SEND = "/home/krishan/Documents/Year_2/UniNotes/Semester2/Networks/test/server/serverFiles/lipsum1.txt"; // you
                                                                                                                                              // may
                                                                                                                                              // change
                                                                                                                                              // this

  public static void main(String[] args) throws IOException {
    FileInputStream fis = null;
    BufferedInputStream bis = null;
    OutputStream os = null;
    ServerSocket servsock = null;
    Socket socket = null;
    try {
      servsock = new ServerSocket(SOCKET_PORT);
      while (true) {
        System.out.println("Waiting...");
        try {
          socket = servsock.accept();
          System.out.println("Accepted connection : " + socket);
          // send file
          File myFile = new File(FILE_TO_SEND);
          byte[] mybytearray = new byte[(int) myFile.length()];
          fis = new FileInputStream(myFile);
          bis = new BufferedInputStream(fis);
          bis.read(mybytearray, 0, mybytearray.length);
          os = socket.getOutputStream();
          System.out.println("Sending " + FILE_TO_SEND + "(" + mybytearray.length + " bytes)");
          os.write(mybytearray, 0, mybytearray.length);
          os.flush();
          System.out.println("Done.");
        } finally {
          if (bis != null)
            bis.close();
          if (os != null)
            os.close();
          if (socket != null)
            socket.close();
        }
      }
    } finally {
      if (servsock != null)
        servsock.close();
    }
  }
}