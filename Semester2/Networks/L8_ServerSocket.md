---
title: Lecture 8
---

# Servers

Clients

- Assume that the server exists, at a perscribed IP address **and port**
- start and connect, communicate (following a prescribed **protocol**), then close and exit

Servers

- run **continuously** on the host
- Listem for client connections on the prescribed port
- Handle connections/disconnections to clients
- Implement the connection protocol


## The ServerSocket class

Contains the only additional functionality required for servers:

- Opens the given port to external connections
- Listens for TCP connections on the given port
- Negotiates the connection between client and server
- Creates a plain Socket object for communication with each client
	- This socket will have **a different port** to the listening port

The remaining functionality, i.e. for communication, is in the Socket class

### The Most Common Constructor

```
public ServerSocket (int port) throws BindException, IOException
```

Port is the port to **listen** for connections

The host is always the local machine i.e. localhost.

- Contrast with Socket, where the remote machine and port is specified

BindException arises when the socket could not be created and bound to that port

BindException is a type of IOException


### Getters

```
public InetAddress getInetAddress()
```

- Returns the address the server is using

```
public int getLocalPort()
````

- The port that the server is listening on
- Usually not very useful, as we normally assign a port in the constructor

There are not (public) set methods for these quantities; they are immutable

- There are set methods for buffer size, timeout, etc

### Useful Methods

````
public Socket accept() throws IOException
```

- Blocks (i.e. waits) until a client connects to the listening port
- Returns a new Socket at a new port.

```
public void close() throws IOException
```

- Frees up the listening port for another application to use
- Alsot closes any connected Socket
- Good programming proactice, although killing the server (Ctrl-C on the command line) will also
  close the connection.


A recipe for a simple server is:

1. Clreate a new ServerSocket on a port
2. Listen for a connection using the accept() method, which waits until a client connects, when it
   returns a Socket object.
3. Set up Socket input and output streams for I/O
4. Communicate using the correct protocol
5. Client, server or both close the connection
6. Return to 2


(Look at slides for examples)



















