---
title: Lecture 7
---

# Sockets

A socket is an abstract input-ouotput device.

It may correspond to a display, keyboard, printer or a data communication line.

It is used by a program to input or output a stream of data.

The use of sockets shields us from the low-level details of network communication.

- i.e. it is an Application layer cnoncept distinct from the Transport layer.

## Networks Specifically

A Socket is one **end point**  of a two way communicatiton link between hosts.

Each end-point is a combination of an IP address and a port number.

A socket is **bound to a port number,** so that the Transport layer cacn identify the recipient in
the Application layer.

- **Immutable** - once the link is made, it cannot be altered without breaking it.
- i.e. no public setPort() / setAddress methods; only getters.

Every TCP connection can be uniquely identified by its endpoints.

## Socket cconnections

There are 7 basic operations:

1. Connect to a remote machine
2. Send data
3. Receive data
4. Close a connection
5. **Bind** to a port
	- Fixed port in an application e.g. 80 for a web server
6. Listen for incoming data
7. Accept connections from remote machines in the bound port 

## Sockets for clients

For clients only the first 4 of these are relevant so these are the only ones that have methods in
the **Socket** class

The remaining 3 are related to **servers**, and have methods in the **ServerSocket** class.

## The Java Socket class

Implements the TCP communicatitons protocol.

A typical session might look like:

- A new socket is created, using the Java Socket constructor
- The socket attempts to connect to a given remote host at the given port.
- The local machine and the remote machine send and receive data
	- The meaning of the data sent depends on the applicatitons
- The connection is two-way; both can send and receive.
- One or both of the machines close the connection.

### Common constructors

Two constructors are most commonly used. As with InetAddress, these are **not** standard
constructors as they perform networking to make the connection.

```
public Socket (String host, int port) throws UnknownHostException, IOException
```

- Tries to create an InetAddress object from the hostnae.
	- If not possble, throws UnknownHostException
- IOException thrown for e.g. unreachable host, routing problem


```
public Socket (InetAddress host, int port) throws IOException
```

- May throw IOException for same reasons as above


