---
title: Lecture 4
---

# DNS

Critical support protocol for other network applications.

A host can be addressed in one of two compatible ways:

- A readable host name (i.e. www.leeds.ac.uk) or
- An IP address (IPv4 or IPv6)

DNS provides a system that maps between thesse two addressin formats.

## Key concepts

**Indirection** - Names replace IP addresses. Rarely use IP addresses directly.

**Hierarchy** - Apparent in IP addresses, their names, and the DNS server structure itself

**Distribution** - No single DNS server contains all names or IP addresses. Scalable.

**Caching** - Local caching of DNS results for re-use.

## Names vs Addresses
Names are easier to remember but provide little information about location.

Addresses are the raw data for network communication - Part of the Network layer protocol.

Addresses are organised hierarchically, and more immediately relate to host location.

## Uniqueness

Every public device on the internet has a unique IP address.

Hosts in private networks i.e. LANs, can use their own internal addresses.

- some ranges of IP addresses reserved for this purpose e.g. 10.\*.\*.\* in v4.

Outward- facing servers i.e. the public hosts visible to the WAN, do have a unique IP address.

- They forward messsages from LAN hosts to the WAN, and vice versa, using a Network Address
  Translation (NAT) table.
- Does this by (ab-)using port numbers

Many to one mapping - One hostname can map to multiple IP addresses:

- popular servers will have multiple IP addresses around the world 
- The DNS server may try to select the 'closest' to the sender
- It may also rotate through the list of IP addresses, to reduce congestion.

For instance, popular websites or CDNs (Content Distribution Networks), will typically have multiple
addresses from which their content can be accessed. How these sites are synchonised is another
matter.

One to many mapping - Multiple names can map to the same IP address.

this is known as **aliaising**.

Especically useful for mail addresses. Can also be used to simplify host names for e.g. websites in
much the same way.

## The Domain Name System

Hierarchical name space:

- Divided into zones
- Distributed across DNS servers
- There is no single, centralised list (so no single point of failure).

Server structure matches the hierarchical name space:

- Root servers
- Top level Domain (TLD) servers
- Authorative DNS servers
- Local DNS servers

(server hierarchy picture on slide 11)

## Root Servers

There are 13 root servers covering the Internet

- Actually many more (> 400), but only 13 IP addresses, i.e. we can only 'see' 13 from one host
- Exception to the 'unique' IP address

These are distributed geographically, although most are in the US. Root server locations are
hard wired into DNS servers.

## TLD servers

Reponsible for top-level domains e.g. .com, .org, .net

Also country domains e.g. .uk, .fr

One server per top-level domain, maintained by a company or organisation

- e.g. Verisign Glocal Registry is reponsible for the .com domain


## Authorative DNS Servers

Organisations who want their sites to be views pubilcly must provide accessible DNS records for
their hosts.

- Either their own authoritative DNS server, or that of a service provider e.g. Universities, ISPs...
- Likely to also maintain a secondary (back-up) DNS server

## DNS Protocol

Uses UDP although can use TCP in special circumstances (i.e. if transferring a large amount of data
from one server to another).

Queries fail if not answered within a set time limit. May retruy until succeeds

**Uses port 53**.

Messages are either 'query' or 'reply' using the same format.

## Using DNS 

A DNS query will pass through the hierarchy:

- Local host is configured to connect to a local DNS server which negotiates with root DNS servre to
  find TLD server
- and then negotiates with TLD server to find **Authoritative **DNS server
- and then negotiates with Authoritative DNS server to resolve host name to IP address
- and then returns this IP address to the local host.

(picture on slide 17 on DNS Queries)

### Recursive and iterative queries

This example includes both recursive and iterative queries:

- The local host asking the local DNS server to resolve a host name is a recursive query.
- The 3 queries from the local DNS to the root, TLD and Authoritative servers in turn is an iterative query 

8 messages required in total for each host name resolution. A very lengthy process!

(image on slide 19)


## DNS Caching

The query process has an overhead:

- Primarily a delay, especically if one of the queries is lost and needs to be re-sent.
- May also cause congestion, as the number of messages for eacch server can become very high.

To improve performance, query results are cached on the local DNS server:

- Cache is checked before the root DNS server is used
- Can cache IP addresses for TLD servers, bypassing the need to access the root server.

(image on slide 21: typo should say cache not auth)

If a query is successful:

- The returned DNS message will include a TTL, or a time to live.
- Will cache until the TTL expires, typically days.

If a query is unsuccessful:

- Overhead for unknown domain names is greater than for known domains, as it requires an exhaustive
  search.
- Caching avoids a repeat of the mistake.
- In this instance, typically has a shorter TTL, of the order of hours

## DNS tools

*nslookup* - resolves hostnames to IP addresses, deprecated but still exists

*host* - Basic resolution of host names and IP addresses 

*dig* resolves host names and IP addresses, can also trance the DNS query, allows for a more
detailed response

