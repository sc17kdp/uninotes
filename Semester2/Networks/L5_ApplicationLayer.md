---
title: Lecture 5
---
# The application layer


## IPv4

Usually written as a 4-byte string of 4 integers

- a.b.c.d
- Each byte takes a value from 0 to 255
- the 4 in IPv4 does not refer to the number of bytes

Some addresses and ranges of addresses have special meaning

Even without this there are only $(256)^4$ possible addresses i.e. about 4.3 billion
- The population of the Earth is currently about 7.5 billion.

### Classful addressing

- Class A 0.\*.\*.\* to 127.\*.\*.\* 
- Class B 128.\*.\*.\* to 191.\*.\*.\*
- Class C 192.\*.\*.\* to 223.\*.\*.\*
- Class D 224.\*.\*.\* to 239.\*.\*.\*
- Class E 240.\*.\*.\* to 255.\*.\*.\*

They vary in size: Class A > Class B > Class C > Class D,E.

#### Problems with the classful model

Various classes are reserved, reducing space

- Class D: Multi-casting (Lecture 15)
- Class E: Reserved for some unspecified 'future use'
- Some special addresses:
	- 0.0.0.0. for 'this' machine
	- 255.255.255.255 is used for broadcasting (device discovery).
	- 127.\*.\*.\* is used for loopback

The least significant 3 bytes were not always managed efficiently.

#### Subnetworks in IPv4

In addition to their first byte ranges, the different classes also allows **subnetworks** to be
defined:

- Class A allows 128 networks, each with $256^3$ hosts
- Class B allows 64 $\times$ 256 = 16384 networks each with 65,536 hosts: a.b.\*.\*
- Class C allows 32 $\times 256^2$ netwroks each with 256 hosts: a.b.c.\*

The idea was each organisation chould choose a subnet.

- Most organisations need more than 256 hosts but less than 25,536.
- Tended to select Class B and under-utilise their subnetwork

### CIDR: (Classless Inter - Domain Routing)

The first fix was to define subnetworks by any number of bits

- greater range of subnetwroks sizes
- Notation a.b.c.d/x with x the number of common bits

For example, 220.10.128.0/20 means all addresses that share their first 20 bits with 220.10.128.0:

- all of the first byte (220)
- all of the second byte (10)
- The most significant 4 bits f the third byte (128-143 inc)

Full range is 220.10.128.0 - 220.10.143.255

### NAT: Network Address Translation

This allows more netwroks, and gaps in Class B netwroks can be filled, but their is a limit on
hosts.

A second fix was for **private** netwroks to have their own *internal* addresses, and only
public-facing servers to has an actual IP address.

- Re-direct messages to/from private hosts using ports
- 10.\*.\*.\* most common

Not regarded as a permanent solution, more of a 'quick fix'


## IPv6

- Uses 16 byte addresses.
- Long term solution to expand the address spacce.
- IPv4 embedded in IPv6 as a special case
- Total of $256^16$ possible addresses
- Even if managed inefficiently, should never run out.

Currently about 20% of availibility of IPv6 to users.

Some legacy systems do not support IPv6.

 - Can wrap IPv6 datagrams into IPv4 datagrams if some intermediate routers only support IPv4
- Known as **tunnelling**

### IPv6 address format

Usually written in **Hexadecimal** with 8 gorups (pairs of bytes) separated by colons.

Can simplify by removing leading zeros in each group. Can further simplify by replacing consecutive
secions of zeros by a double colon.

You can only have *one* double colon in an address, otherwise it could be ambiguous.

### IPv6 subnetworks

CIDR also applies to IPv6 with the same slash notation.

The private address used to denote machines on the same elocal netwrok can use the address:

- 10.\*.\*.\* or 10.0.0.0/8 in IPv4
- fc00::/8 in IPv6


