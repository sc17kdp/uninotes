---
title: Lecture 12
---

# Non-Blocking I/O


The methods of java.io such as read() and write() are **blocking**

- The thread associated with that operation will be **idle** until a connection is made, data is
  received etc.
- Wasteful o fresources but easy to implement.

The associated Java classes allow a **timeout** to be set.

- Exceeding this time throws an `InterruptedIOException`, derived from IOException.
	- e.g. SocketTimeoutException from last lecture.
- Must allow for this behaviour in our application logic (retry? give up?)
- Still idle until the exception is thrown.

We can also consider the **non-blocking** I/O features in java.nio.

These were orignally designed to support **high performance servers**

- Long-lived clients still a problem for thread-pools.
- Blocking I/O mandates the use of threads.
- Multiple threads can introduce synchronisation problems (if they interrract)

java.nio allows multiple client handling **in a single thread**.

- Application logic is more complex

## Primary Java classes

The most important classes/interfaces for non-blocking I/O are:

- **Channels**, a container for I/O objects (i.e. Socket) (actually an interface)
- **Selector**, manages channels
- **Buffers**, a container for data

`Buffer` is defined in java.nio.

`Channel` and `Selector` are defined in java.nio.channels.

## Buffers

The Buffer class is a **non-stream based I/O container**

- Used by Channel for I/O.
- Data moved as larger blocks, rather than small bytes.
- Are switchable, i.e. can be bi-directional (contrast with the I/O streams in java.io which are
  **uni-directional**i

## Buffers and OS buffers

Instances of Buffer are lower level constructs than standard I/O streams.

Map naturally to OS system-level buffers.

Efficient use of buffer data in **place**.

## Channels

A Channel represents a **pollable** I/O target. e.g. Socket

A Channel can be configured to be either blocking or non-blocking. We focus on non-blocking here.

A Channel permits two way communication. Can read or write from its associated Buffer.

A Channel registers with a Selector. Selector manages the channel and monitors its state, i.e. if it
is readable, writable etc.

## Server channel code

Open a ServerSocketChannel (implements the Channel interface; defined in java.nio.channels):

`ServerSocketChannel serverChannel = ServerSocketChannel.open();'

Configure to be non-blocking (as it defaults to blocking);

`serverChannel.configureBlocking(false);`

Bind to the listening port:

`serverChannel.socket().bind( new InetSocketAddress(portNum));`

Note this is identical to the blocking I/O method.

## Selectors

The Selector class manages access to a set of Channels.

- Of any type; server or client connections.

The select() method queries all channels **registered** with this selector.

- Returns any **ready** channel.
- Can be ready for reading, writing, or accepting (a new client)

### Registering a channel with a selector

Use the `Selector.open()` factory method, which returns the system's degault selector provider:

`Selector selector = Selector.open();`

Register using the register() method of the channel.

`serverChannel.register(selector,SelectionKey.OP_ACCEPT));`

Registered channels are known as **keys**.

Common options are:

- SelectionKey.OP_ACCEPT for a listening ServerSocket.
- SelectionKey.OP_READ to read data
- SelectionKey.OP_WRITE to write data

### Channel state

A channel can be idle or ready for one of several operations.

For a ServerSocketChannel, there is only really one:

- Client connection requested (SelectionKey.OP_ACCEPT)

For each of the client SocketChannels:

- Reading from a buffer (SelectionKey.OP_READ)
- Writing to the buffer (SelectionKet.OP_WRITE)

We use the Selector to return ready channels and a key to their state.

- Selectors can manage a collection of channels with various states.

## Pros and Cons of non-blockig I/O

The single-threaded non-blocking approach is **conceptually** simpler, but in reality harder to
implement.

- Requires care
- Switching between read and write can be complex.

When first released in the 1990's, non-blockinig architectures dramatically out-performed blocking
ones.

- Java was slow to release nio.
- By the time it came out, many advantages were lost.

### Non-blocking I/O today

By the 2000's machine and operating system improvements have reduced the advantage to the point
that:

- Commodity servers can support roughly 10,000 threads.
- Multi-threaded, stream based I/O servers can out-perform non-blocking architectures by as much as
  30%.

May still outperform multi-threaded servers in situations with:

- A large number of threads that are long-lived but low activity
- Blocking IO would waste resources in this case
