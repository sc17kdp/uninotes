---
title: Lecture 3
---

# Ports

Data is sent from the application layer down through the **protocol stack**, and up the stack at the
other side once it reaches the destination.

Applications are linked to the Transport later via **network ports**.

* Different port numbers are associated with different application layer processes
* This allows multiple applications to run simultaneously on the same host with thee same IP
  address.
* Purely software, provided by the OS.
* Can be allocated to a particular service e.g. email, HTTP

## Sending and receiving

Converting host-to-host delivery to process-to-process delivery is the job of the **Transport layer**.

Multiplexing - Sending

Demultiplexing - Receiving

Applications typically publish the port they are **listening to ** (receiving on).
* A sending process will attempt to establish a connection using the published port number
* At that point a new port is allocated **dynamically** to maintain that specific communication.

## Available ports
Ports 1-65535 are available on any given host.

* 16-bit unsigned int, with 0 not allowed
* TCP and UDP ports and independent.

Ports are characterised by:

* Ports 1-1023 are **reserved**; approved by IANA (Internet Assigned Numbers Authority)
* Commonly used ports lie outside this range, i.e. 1024-65535
* We can use any freely availble ports in this range

### Remote login and file transfer

*telnet* (teletype network) - Port 23

* The original client-server communication protocol but rarely used now as it is insecure

*ssh* (Secure shell) - Port 22

*ftp* (file transfer protocol)

* Port 21 for commands (dir, put, get, etc..)
* Port 20 used for data (the original use; slow)

### Email

*smtp* (simple mail transfer protocol) - Port 25

* Used for sending mail

*imap* (internet message access protocol) - Port 143

* Used to access mail.

*pop3* (post office protocol) - Port 110

* Also used to access mail.

### Web

*http* (hypertext transfer protocol) - Port 80

* insecure

*https* - Port 443

* Secure version of http, requiring authentification.

## Network communication
At the application layer, the data to be sent to the destination host and port nummber is packaged
for transport.

The Transport layer is oblivious to the subnet that enables the communication

* adds a header that includes information such as port numbers (for the sender and receiver), checksum

At the Network layer and below, the subnet is explicitly involved

* e.g. The Network layer determines which network nodes the message will pass through

**The application and transport layers are network oblivious**

There are two key protocols for the Transport layer: UDP and TCP. They differ in the info provided
in their headers, the reliability of service and performance.

They both have a checksum for data integrity.

## UDP - User Datagram Protocol
Connectionless protocol - Each message is independent.

Prepends an 8-byte header to the message (2 bytes each):

* Destination port number
* Source port number
* Message length
* Checksum

The ports identify the processes on both the **source and destination hosts.**

Has a checksum for data integrity but simply discards data segments that have been corrupted.

### Header 
There are no host addresses in the header. This is the responsibility of the Network layer.

## Uses of UDP

Important use is to access a DNS server to map a readable internet address to an IP address

Also useful for **real-time multi-media**, often wrapped into the Real-time Transport Protocol (RTP)

* Sits between application and Transport layers
* UDP packets are numbered such that receiver can determine if packets are missing.
* No correction or retransmission
* Receiver can interpolate lost data


## TCP - Transmission Control Protocol

**Connection - oriented protocol** i.e. maintains a persistent connection between two hosts.

* Ensures reliable data transfer, Possibly by requesting re-transmission of lost or corrupt
  segments.
* Also provides a degree of congestion control.

20 - Byte header with:

* Sequence and acknowledgement numbers (4-bytes each)
* Bits used for maintaining the connection 
* Some specialist fields

The remaining fields are the same as UDP.

The squence and acknowledgement numbers are used to guarantee ordering, and to check for missing
packets.

(Image on slide 18)

## Network layer -IP

Forwards Transport layer data with a 20-byte (IPv4) / 40-byte (IPv6) header

* Source and destination address (IPv4 or IPv6)
* Protocol (TCP or UDP) 
* time to live
* checksum for thee header integrity (IPv4)

The time-to-live decrements whenever the datagram passes through a netwrok node. Once it reaches
zero, the message is discarded. This avoids datagrams that circulates forever.

## Link and physical layers

Frames of data are forwarded to the Physical layr with a 16-byte header containing:

* MAC address (Media Access Control)
* Checksum

The MAC address enables the frame to 'hop' to the next network device.

The checksum is for frame integrity.

## Network Transmission

The route from source to destination passes through multiple devices (typically).

Network -layer **routers** determine the path between hosts.

Link layer **bridges** and **switches** forward frames between network components.

Physical layer **repeaters** and **hubs** regenerates the signl, thereby enabling a greater range.
