import java.net.*;
import java.io.*;
import java.util.Date;
import java.util.concurrent.*;

public class Server {
	public static void main(String[] args) throws IOException {

		ServerSocket server = null;
		ExecutorService service = null;

		// Try to open up the listening port
		try {
			server = new ServerSocket(8888);
		} catch (IOException e) {
			System.err.println("Could not listen on port: 8888.");
			System.exit(-1);
		}

		// Initialise the executor.
		service = Executors.newFixedThreadPool(10);

		// For each new client, submit a new handler to the thread pool.
		while (true) {
			Socket client = server.accept();
			service.submit(new ClientHandler(client));
		}

	}

}

class ClientHandler extends Thread {
	private Socket socket = null;
	private OutputStream os = null;
	private InputStream is = null;

	public ClientHandler(Socket socket) {
		super("ClientHandler");
		this.socket = socket;
	}

	public void run() {
		try {
			os = socket.getOutputStream();
			is = socket.getInputStream();
			PrintWriter out = new PrintWriter(os, true);
			BufferedReader in = new BufferedReader(new InputStreamReader(is));
			InetAddress inet = socket.getInetAddress();

			String inputLine, filesize, filename;

			while ((inputLine = in.readLine()) != null) {
				System.out.println(inputLine.substring(3));
				if (inputLine.equals("list")) {
					// list all files in serverFiles
					writeLog(inet, inputLine);
					getList(out);
					break;
				} else if (inputLine.substring(0, 3).equals("get")) {
					// send file to client
					filename = in.readLine();
					writeLog(inet, inputLine.substring(0, 3));
					sendFile(filename);
					break;

				} else if (inputLine.substring(0, 3).equals("put")) {
					// get file from client
					writeLog(inet, inputLine.substring(0, 3));
					// get the file size
					filesize = in.readLine();
					System.out.println("File size: " + filesize);
					// receives the file
					receiveFile(inputLine.substring(3), Integer.parseInt(filesize));
					break;
				} else {
					System.out.println("Not a valid command");
					break;
				}

			}

			socket.close();

		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}

	public void sendFile(String fname) throws IOException {
		BufferedInputStream bis = null;
		try {
			// file path
			File myFile = new File("serverFiles/" + fname);
			byte[] data = new byte[(int) myFile.length()];
			bis = new BufferedInputStream(new FileInputStream(myFile));
			bis.read(data, 0, data.length);
			System.out.println("Sending " + fname);
			os.write(data, 0, data.length);
			os.flush();
			System.out.println("Done.");
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}

	public void receiveFile(String fname, int fileSize) throws IOException {
		int current = 0;
		BufferedOutputStream bos = null;
		int sizeOfFollow = 6022386; // DONT HARD CODE THIS IN
		try {
			byte[] data = new byte[sizeOfFollow];
			bos = new BufferedOutputStream(new FileOutputStream("serverFiles/" + fname));
			// loads file into array
			while ((current = is.read(data)) > 0) {
				bos.write(data, 0, current);
			}
			bos.flush();
			System.out.println("File " + fname);
		} catch (Exception e) {
			System.out.println("Error Message: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (bos != null)
				bos.close();
			if (socket != null)
				socket.close();
		}

	}

	public void getList(PrintWriter out) {
		File folder = new File("serverFiles");
		File[] fileList = folder.listFiles();
		for (int i = 0; i < fileList.length; i++) {
			if (fileList[i].isFile()) {
				out.println(fileList[i].getName());
			}
		}
	}

	public void writeLog(InetAddress inet, String inputLine) {
		try {
			// write log
			Date date = new Date();
			BufferedWriter bw = new BufferedWriter(new FileWriter("log.txt", true));
			bw.write(date.toString() + " : " + inet.toString() + " : " + inputLine + "\n");
			bw.close();

		} catch (IOException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
}