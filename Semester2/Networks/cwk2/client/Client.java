
import java.net.*;
import java.nio.Buffer;
import java.io.*;

public class Client {

	private Socket socket = null;
	private PrintWriter sockOut = null;
	private BufferedReader sockIn = null;
	private OutputStream os = null;
	private InputStream is = null;

	public static void main(String[] args) {
		String command = args[0];
		Client client = new Client();
		String fname = null;
		client.startClient();
		try {
			// list - lists all the files in serverFiles
			if (command.equals("list")) {
				// call the list command
				client.list();
			}
			// get fname - retrieves fname and saves it in clientFiles
			if (command.equals("get")) {
				// call the get command
				fname = args[1];
				client.get(fname);

			}
			// put fname - sends fname and saves it in serverFiles
			if (command.equals("put")) {
				// call the put command
				fname = args[1];
				client.put(fname);

			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}

	}

	public void startClient() {

		try {

			// try and create the socket
			socket = new Socket("localhost", 8888);

			// chain a writing stream
			os = socket.getOutputStream();
			sockOut = new PrintWriter(os, true);

			// chain a reading stream
			is = socket.getInputStream();
			sockIn = new BufferedReader(new InputStreamReader(is));

		} catch (UnknownHostException e) {
			System.err.println("Don't know about host.\n");
			System.exit(1);
		} catch (IOException e) {
			System.err.println("Couldn't get I/O for the connection to host.\n");
			System.exit(1);
		}
	}

	public void list() {
		// write to server
		String fromServer;
		try {
			sockOut.println("list");
			while ((fromServer = sockIn.readLine()) != null) {
				// echo server string
				System.out.println("Server: " + fromServer);
			}
			sockOut.close();
			sockIn.close();
			socket.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}

	}

	public void get(String fname) throws IOException {
		int bytesRead;
		FileOutputStream fos = null;
		BufferedOutputStream bos = null;
		int fileSize = 6022386;
		try {
			sockOut.println("get");
			sockOut.println(fname);
			byte[] data = new byte[fileSize];
			InputStream is = socket.getInputStream();
			// saves the file in client files
			bos = new BufferedOutputStream(new FileOutputStream("clientFiles/" + fname));
			bytesRead = is.read(data, 0, data.length);

			bos.write(data, 0, bytesRead);
			bos.flush();
			System.out.println("File " + fname);
		} catch (IOException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			if (fos != null)
				fos.close();
			if (bos != null)
				bos.close();
			if (socket != null)
				socket.close();
		}
	}

	public void put(String fname) throws IOException {
		BufferedInputStream bIn = null;
		BufferedOutputStream bOut = null;
		int fileSize = 6022386;
		try {
			sockOut.println("put" + fname);
			File fileToSend = new File("clientFiles/" + fname);
			System.out.println(fname);
			// sends file size to the servers
			sockOut.println(fileToSend.length());
			// checks if the file exists
			if (!fileToSend.exists()) {
				System.out.println("File doesn't exist");

			} else {
				System.out.println("File exists");
				byte[] data = new byte[fileSize];
				bIn = new BufferedInputStream(new FileInputStream(fileToSend));
				bIn.read(data, 0, data.length);

				bOut = new BufferedOutputStream(os);
				System.out.println("Sending " + fname);
				// Send the file
				bOut.write(data, 0, data.length);
				bOut.flush();
				System.out.println("Done.");

				System.out.println("File Transfered...");
			}

		} catch (Exception e) {
			System.out.println("Client Exception : " + e.getMessage());
		} finally {
			if (bIn != null)
				bIn.close();
			if (bOut != null)
				bOut.close();
			if (socket != null)
				socket.close();
		}

	}

}