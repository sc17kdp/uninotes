import java.io.IOException;
import java.net.*;
import java.util.ArrayList;

/**
 * The coursework 1 class resolves the ip address from the host name given Then
 * prints out the relavent information Then checks if there are two ip addresses
 * with a common hierarchy and prints out their common parts of the ip addresses
 */
public class Coursework1 {

    private InetAddress inet = null;
    private static ArrayList<InetAddress> ip4inets = new ArrayList<InetAddress>();

    private final int timeout = 5000;

    /**
     * resolves the host names and address
     * 
     * @param host is the host name given by the user on the command line
     */
    public void resolve(String host) {
        try {
            // tries to create an instance of InetAddress but if it fails then it will throw
            // an UnknownHostException
            inet = InetAddress.getByName(host);

            // prints the resultls of the three getter methods
            System.out.println("Host name : " + inet.getHostName());
            System.out.println("Canonical Host name : " + inet.getCanonicalHostName());
            System.out.println("IP Address: " + inet.getHostAddress());
            // checks if it is ipv4 or 6
            checkIPversion(inet);
            // checks if it is reachable
            checkReachable(inet);
            System.out.println("========================================");
            // catches UnknownHostException
        } catch (UnknownHostException e) {
            System.out.println("The host is unknown");
            e.printStackTrace();
        }
    }

    /**
     * Checks if the ip is ipv4 or ipv6
     * 
     * @param inet is the inet address given
     */
    public void checkIPversion(InetAddress inet) {
        if (inet instanceof Inet6Address) {
            // It's ipv6
            System.out.println("IP Version: IPv6");
        } else if (inet instanceof Inet4Address) {
            // It's ipv4
            System.out.println("IP Version: IPv4");
            // adds the inet to the arraylist of IPv4 inets
            ip4inets.add(inet);
        }
    }

    /**
     * checks if it is reachable
     * 
     * @param inet is the inet address given
     */
    public void checkReachable(InetAddress inet) {
        // checks if it is reachable or not
        try {
            System.out.println("Reachable: " + inet.isReachable(timeout));
        } catch (IOException e) {
            System.out.println("IO exception");
            e.printStackTrace();
        }
    }

    /**
     * works out at which level ip addresses have a common hierarchy
     * 
     * @param ip4inets is the ArrayList of all of the ipv4 addresses
     */
    public static void highLevelIP(ArrayList<InetAddress> ip4inets) {
        Boolean[] byteArray = { true, true, true, true };
        byte[] bytes1 = ip4inets.get(0).getAddress();
        String commonIP = "";

        for (int i = 1; i < ip4inets.size(); i++) {
            byte[] currentByte = ip4inets.get(i).getAddress();
            compAddresses(bytes1, currentByte, byteArray);
        }
        for (int j = 0; j < byteArray.length; j++) {
            if (byteArray[j]) {
                commonIP += (bytes1[j] & 0xFF);
            } else {
                for (int k = j; k < 4; k++) {
                    // finishes off the rest of the ip address with "*"
                    commonIP += "*";
                    if (k != 3) {
                        // stops there being an extra "." at the end
                        commonIP += ".";
                    }
                }
                // breaks out of the loop
                break;
            }
            if (j != 3) {
                // stops there being an extra "." at the end
                commonIP += ".";
            }
        }
        // checks that they are not all *'s
        if (commonIP.charAt(0) != '*') {
            System.out.println("Common IP address hierarchy: " + commonIP);
        }
    }

    /**
     * Compares two addresses and checks at what point they differ
     * 
     * @param bytes1 bbyte array of first ip address
     * @param bytes2 byte array of second ip address
     * @param bArray boolean array of whether each byte is the same or not
     */
    public static void compAddresses(byte[] bytes1, byte[] bytes2, Boolean[] bArray) {
        // loops through the 4 bytes of the ip address'
        for (int b = 0; b < 4; b++) {
            if (bytes1[b] != bytes2[b]) {
                // if two of the bytes are not equal then it sets that byte to false
                bArray[b] = false;
            }
        }
    }

    public static void main(String[] args) {
        Coursework1 cwk = new Coursework1();
        for (int i = 0; i < args.length; i++) {
            // takes in as many command line arguments as there are
            cwk.resolve(args[i]);
        }
        // gets the highest level of ipV4 addresses
        if (ip4inets.size() > 1) {
            highLevelIP(ip4inets);
        }
    }
}