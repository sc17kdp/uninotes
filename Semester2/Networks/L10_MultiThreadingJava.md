---
title: Lecture 10
---

# Multithreading in java

We have two options for threading in Java

1. Subclass the Thread class
2. Use the Runnable interface

In both cases, you will put your code into:

`public void run()`

Request a new thread to execute the code in `run()` by calling: `public void start()`

Other sometimes useful methods:

- `join()`, wait for the thread to finish
- `sleep (long millisecs)`, causes the thread to sleep.


## Scheduler

Threads do not start running the instant we create them.

- When a thread runs is managed by a component of the OS called the scheduler
- Allocates core time dynamically, depending on other applications, background tasks etc.

Since you cannot cocntrol when other applications or background tasks are active, a parallel program
may generate different results each time it is executed.

- This **non - deterinism** is usually undesirable
- Can be corrected with **synchronisation**


(Java code on slides 6 and 7)

## Inheritancce vs Interface

The interface approach is more general:

- Can inherit from a seperate class
- Can implement multiple interfaces, but cannot have multiple inheritance.
- Threading is a property of a class, not essential to its definition.

Inheriance is simpler, but our class must be a descendent of `Thread` 

- May be very slight performance improvement relative to the interface approach (less 'indirection')

## General issues in thread programming

Two important multi-threading issues to be aware of are **data races** and **synchronisation**.

A data race is when two threads read and write the same data

- The result of the **read** depends on whether or not the other thread has changed the data.
- Since we cannot control when threads run, the result may vary each time.

### Synchronisation

If one thread cannot continue until another one finishes, mut **synchronise**. There are two common
approaches:

1. Calling t.join() will pause the calling thread until its subthread t has finished (below).
2. Defining a synchronised block or method 

Can specify blocks of code that only one thread can enter at a time. There is a single lock for the
block with a thread must acquire before entering the block. It reliquishes the lock when leaving the
block. In Java this is achieved using
`synchronised(object)`

Without `synchronised()` the print() statements for each thread could become **interleaved.**. 

Java also allows you to synchronise methods. Only one thread can enter the method at a time.

Note that use of synchronized can severely affect performance. Other constructs such as atomic types
could be considered.

OutputStram objects do not synchronise in general, so e.g. outputting to the same log file may
require synchronisation.

For our simple applications the threads will be **independent**.

- Data races should not arise
- Synchronisation usually not required, but may be, depending on how you implement, e.g. output to a
  log file.

## Client - Server architecture

The software architecture we choose should be driven by the **application requirements**. 

Relevant considerations are:

- How many clients do we expect (concurrently)?
- How long are clients connected for?
- What type of protocol we are implementing? For instance, is communication contant, or are there
  idle periods.

## Thread-per-client model

Uses a **single thread per connected client**.

- Creates the thread as the connection is made.
- Destroys the thread once the connection closes.

Assumes the resources are available for many threads:

- Each thread requires some **local data**
- Creating and destroying threads also costs **CPU cycles**.

At large enough scale (i.e. large number of threads), these costs outweigh the benefits

- Adding more threads makes the system less responsive.
- Typically limited to 100's to 1000's of unique threads on a typical desktop

Pros:

\+ Simple to understand and implement

\+ Improvement over the non-threaded version we saw earlier

Cons:

\- Resources can potentially grow without limit: Could try to estimate a maximum number of clients

\- A thread is created and destroyed for each client. Although a much smaller overhead than for
spawning processes, it can be significant.

\- Better to create threads once and re-use for multiple clients.


