---
title: Lecture 15
---

# UDP - Sending the same data to more than one client

Our multi threaded and non-blockig I/O server implementations were capable of handling multiple
clients. Either TCP or UDP.

Sometimes we want to send the same message to all users.

Can send to a **network**, called broadcasting.

Cam also send to a **group of hosts**. Known as **multicasting**.

The server application sends one message but it is routed to multiple clients. Requires additional
processing by routers.

## Broadcasting

The Network layer supports broadcasting.

- For instance, if a device is added to a network it first needs an IP address.
- Initially communicates with local network using broadcasting.
- Part of DHCP = Dynamic Host Configuration Protocol.

The IPv4 address 255.255.255.255 is reserved for local broadcasting.

- Messages will not be forwarded by a router.
- Other hosts on the local network choose whether to listen.
- No IPv6 equivalent; instead multi-casts to ‘all hosts.’

## Multicast

For the client-server programming model:

The server delivers duplicate data to multiple clients

- but only sends one copy on the network
- Low risk of congestion in the link between client and the first (edge) router.

The clients each receive a single copy of the data.

- No change to versions considered before.

### Multicast architectures

A simplified client-server protocol would be something like:

The server sends a stream of data.

- Continuous (always on)

The client connects, receives data, and disconnects.

- Can connect and disconnect at any point in the stream.

Such an architecture could be considered fr e.g. broadcast of a live event.

- Not for on-demand services, where each client requires different data streams.

## TCP or UDP

TCP:

- Requires a maintained connection to each client.
- Requires separate acknowledgement from each client for each message and possible error handling.
- Impractical to implement.

UDP:

- Requires a mechanism for routing a single packet to multiple clients.
- The client must perform any further error handling.
- Java implements multicast using UDP.

## Multicast addresses

A set of IP addresses are available for multicast communication.

- IPv4: Class D: 244.0.0.0 to 239.255.255.255.
- IPv6: Prefix ff00::/8.
- A small number have been assigned, but have had limited penetration thus far,

Each address represents a **multicast group**

- Servers send packets to this address
- Clients listen to this address; they are **not** connected.

## Routing

Multicast relies on the **router** to handle these addresses. The routing process is more complex.

Transparent to application programmers:

- It is a service supplied by lower levels in the protocol stack.
- Standard UDP-based network programming

In fact, the server could be `DatagramSocket` with packet addresses to the multicast address.

However, the client must be a multicast socket as it needs to join the group.

## Scope

How far do we want our message to travel?

- There is no direct connection.
- We do no know who is listening.
- Without some sort of control, packets could proliferate even without multicasting.

The DatagramPacket has a field called TTL = Time To Live.

- Counts and limits router hops before the packet is discarded.
- For instance, TTL=0 is the localhost, TTL=1 might be the
School, TTL=48 is country-wide and TTL=225 is worldwide (approximately).


## The MulticastSocket class.

The primary class for multicasting in Java is `MulticastSocket`:

- Defined in java.net.
- extends DatagramSocket.
- Inherits the UDP communication model.
- Has additional capabilities for joining multicast groups.
- Is specified by (and requires) a class D IP address and any standard UDP port number.
- Communications purely identified by IP, not IP/port.


