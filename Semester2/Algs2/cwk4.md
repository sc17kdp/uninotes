
 # Algorithms 2 Coursework 4

 ## Pseudocode

 '''
    INPUT: 
        A - A list of integers

    OUTPUT:
       Max-  The maximum XOR of two numbers in the input list
 '''

 METHOD:
    Max <- 0
    For every pair of numbers in A (x,y):
        if (A[x] BITWISE XOR A[y] > Max):
            Max = A[x] BITWISE XOR A[y]
    return Max


## Proof of correctness

Due to the rules that were given if a stock is in A and B or it is in neither A nor B then then stock cannot appear in C, otherwise the stock is in C. 

We can write this out as follows if we use 'a' and 'b' to represent individual stocks in A and B respectively:
    (a OR b) AND (NOT a OR NOT b)

This is the definition of the bitwise XOR operation.


Therefore if we XOR each pair of portfolios we can find the pair that gives the maximum value once XOR'd and store this in portfolio C.

Each time a new pair of portfolios is XOR'd this value is compared to the current maximum. If this value is larger than the current maximum then the maximum is updated. This means that once every pair of portfolios is compared the maximum resultant portfolio will be stored as the current maximum, which will give you portfolio C.


