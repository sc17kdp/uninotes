import sys
# initialise variables
p = []
c = 0
q = sys.argv[1]
# remove first and last brackets
q = q.replace('[', '')
q = q.replace(']', '')

#append the input to a list of integers
for s in q.split(','):
    p.append(int(s))

#find the maximum xor of two pairs of numbers
for a in range(0, len(p)):
    for b in range(a, len(p)):
        if ((p[a] ^ p[b]) > c):
            c = p[a] ^ p[b]


print(c)
